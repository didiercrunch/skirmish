# Skrimish

![logo](https://erpscan.com/wp-content/themes/erpscancom/img/hq__amsterdam.svg "Skrimish")

A stock exchange simulator.  The scope of this project is not yet defined but it should
allow end user(s?) to exchange securities via a kind of stock exchange


## Getting Started

### Development

In development, you need nothing else than [clojure](https://clojure.org/)
and [leiningen](https://leiningen.org/).  The easiest way to install clojure is
to make sure you have java (8 and above) and install leiningen.

### Production

You will need plenty of things, the majority of them are yet to define.
But, a good kafka deployment will be required.  Probably a database,
hopefully datomic.


### Configuration
All configuration are set in the `resources/config.edn` file.
Configuration uses [aero](https://github.com/juxt/aero).  The configuration
file should be self documented.


### Technologies

#### Big architecture

* [docker](https://www.docker.com/) because it is trendy.
* [docker-compose](https://docs.docker.com/compose/) to manage system dependencies.
* [jvm](https://en.wikipedia.org/wiki/Java_virtual_machine) because nothing beats it.

#### Server
* [clojure](https://clojure.org/) for programming language
* [kafka](http://kafka.apache.org/) for message broker/database
* [datomic-free](https://www.datomic.com/) for transaction needs
* [clickhouse](https://clickhouse.yandex) to store the market data

#### In the browser
* [clojure script](https://clojurescript.org/) for programming language
* [reagent](https://reagent-project.github.io/) as frontend framework.  Simple and powerful.
* [metricsgraphicsjs](http://metricsgraphicsjs.org/) for complex charts
* [dc-js](http://dc-js.github.io/dc.js/) for some charting.  Looks awesome!.


### Architecture shenanigans

As any personal project, over engineering it the only solution.  Moreover the
scope of the project is unknown.  Consequently, there is a plethora of
micro-servicesh.


#### Micro services

A micro service does one thing, and does it well.  Micro services communicate to the world
via REST api and collaborate together via a global state kept in kafka.

| name                        | description                                                               |  REST |
|:----------------------------|:--------------------------------------------------------------------------|:-----:|
| `skirmish.exchange`         | Exchange engine.  Knows nothing about what it trades.                     | `yes` |
| `skirmish.exchange-gateway` | Public api to `skirmish.exchange`.  Dispatch an order to the proper place | `no`  |
| `skirmish.ledger`           | Source of truth for who owns what.                                        | `yes` |
| `skirmish.bookkeeper`       | Source of truth for the marker data.  Publishes market data to the world. | `yes` |



## License

Copyright © 2020 didier amyot

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
