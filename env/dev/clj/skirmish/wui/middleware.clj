(ns skirmish.wui.middleware
  (:require [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [prone.middleware :refer [wrap-exceptions]]))

(defn custom-dev-middleware [handler]
  (fn [request]
    (let [response (handler request)]
      response)))

(defn wrap-middleware [handler]
  (-> handler
      (wrap-defaults site-defaults)
      custom-dev-middleware
      wrap-exceptions))
