(ns ^:figwheel-no-load skirmish.wui.dev
  (:require
    [skirmish.wui.core :as core]
    [devtools.core :as devtools]))

(devtools/install!)

(enable-console-print!)

(core/init!)
