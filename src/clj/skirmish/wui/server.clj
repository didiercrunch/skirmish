(ns skirmish.wui.server
  (:require [skirmish.wui.handler :refer [app]]
            [org.httpkit.server :refer [run-server]])
  (:gen-class))

(defn -main [config securities-api exchange-apis]
  (let [port 3000]
    (run-server app {:port port :join? false})))
