(ns skirmish.wui.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [hiccup.page :refer [include-js include-css html5]]
            [skirmish.wui.middleware :refer [wrap-middleware]])
  (:gen-class))

(def mount-target
  [:div#app
   [:h3 "ClojureScript has not been compiled!"]
   [:p "please run "
    [:b "lein figwheel"]
    " in order to start the compiler"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:title "Skirmish"]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css  "/css/site.css");  use "/css/site.min.css" in production
   (include-css "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css")
   (include-css "https://fonts.googleapis.com/css?family=Frijole%7CKarma")])

(defn homepage []
  (html5 {:class "has-background-black"}
    (head)
    [:body.body-container
     mount-target
     (include-js "/js/app.js")
     (include-js "https://sidecar.gitter.im/dist/sidecar.v1.js")]))

(defn loading-page []
  (html5
    (head)
    [:body {:class "body-container"}
     mount-target
     (include-js "/js/app.js")]))

(defn cards-page []
  (html5
    (head)
    [:body
     mount-target
     (include-js "/js/app_devcards.js")]))

(defroutes routes
  (GET "/" [] (homepage))
  (GET "/:client-path" [] (homepage))
  (GET "/:client-path-1/:client-path-2" [] (homepage))
  (GET "/:client-path-1/:client-path-2/:client-path-3" [] (homepage))
  (GET "/about" [] (loading-page))
  (GET "/cards" [] (cards-page))
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
