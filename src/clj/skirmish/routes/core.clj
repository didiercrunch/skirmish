(ns skirmish.routes.core
  (:require [skirmish.routes.api :as routes-api]
            [compojure.core :as compojure]))


(def ledger-routes {
                    :name :ledger
                    :domain "http://localhost:1604" ;; should somehow be in some config
                    :path "/api/1/ledger"
                    :routes [{:name :home :path "/"}
                             {:name :accounts :path "/accounts"}
                             {:name :account :path "/accounts/:account-id"}
                             {:name :securities :path "/securities"}
                             {:name :security :path "/securities/:sec-id"}
                             {:name :transactions :path "/transactions"}]})

(def all-routes [ledger-routes])

(def get-context (partial routes-api/get-context all-routes))

(def get-compojure-path (partial routes-api/get-compojure-path all-routes))

(def get-path-for (partial routes-api/get-path-for all-routes))

(defmacro GET [path args & body]
  (compojure/compile-route :get
                           (get-compojure-path path)
                           args
                           body))

(defmacro POST [path args & body]
  (compojure/compile-route :post
                           (get-compojure-path path)
                           args
                           body))

(defmacro PUT [path args & body]
  (compojure/compile-route :put
                           (get-compojure-path path)
                           args
                           body))

(defmacro context [path args & routes]
  (let [resolved-path (get-context path)]
    (concat `(compojure/context ~resolved-path ~args) routes)))
