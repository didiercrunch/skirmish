(ns skirmish.routes.api)

(defn get-app-for [all-routes k]
  "An app consists on a name, a prefix and some routes."
  (let [app-name (-> k namespace keyword)
        app (first (filter #(= app-name (:name %)) all-routes))]
    app))

(defn- get-route-for [all-routes k]
  (let [app (get-app-for all-routes k)
        route-name (-> k name keyword)
        route (first (filter #(= route-name (:name %)) (:routes app)))]
    route))

(defn get-compojure-path [all-routes k]
  "Returns what compojure expects to have."
  (->> k (get-route-for all-routes) :path))

(defn- replace-part [part params]
  (if (clojure.string/starts-with? part ":")
    (-> part (subs 1) keyword params)
    part))

(defn- path-for [p params]
  (let [parts (clojure.string/split p #"/")]
    (clojure.string/join "/" (map #(replace-part % params) parts))))

(defn get-context [all-routes c]
  (let [get-app #(get-app-for all-routes %)]
    (-> c
        name
        (keyword "some-name")
        get-app
        :path)))

(defn get-path-for
  "Returns a link to a location by name."
  ([all-routes k]
   (get-path-for all-routes k {}))
  ([all-routes k params]
   (let [app (get-app-for all-routes k)
         route (get-route-for all-routes k)
         p (str (:path app) (:path route))
         domain (get app :domain "")]
     (str domain (path-for p params)))))

