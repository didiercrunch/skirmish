(ns skirmish.ledger.data-layer
  (:require [datomic.api :as d]
            [infix.macros :refer [infix]]
            [slingshot.slingshot :refer [throw+]]))

(defn get-all-account-public-key [db]
  (d/q '[:find ?pk :where [?e :skirmish.ledger/owner-public-key ?pk]] db))

(defn compute-simple-face-value [amount daily-interest-rate deposit-date value-date]
  (let [min-in-milliseconds (infix 60 * 1000)
        minute-interest-rate (infix daily-interest-rate / (24 * 60. * 10000.))
        ite (quot (- (. value-date getTime) (. deposit-date getTime)) min-in-milliseconds)
        base (infix 1 + minute-interest-rate)]
    (infix amount * base ** ite)))

(defn- in-interval? [[_ start end] x]
  (let [x-time (. x getTime)]
    (and (<= (. start getTime) x-time)
         (< x-time (. end getTime)))))

(defn- larger-then-interval? [[ _ start end] x]
  (and (<= (. x getTime) (. end getTime))
       (<= (. x getTime) (. start getTime))))

(defn- split-interest-rate-intervals [interest-rates-intervals deposit-date value-date]
  (let [xf (comp (drop-while #((comp not in-interval?) % deposit-date))
                 (take-while #((comp not larger-then-interval?) % value-date)))
        ret (into [] xf interest-rates-intervals)
        ret (assoc-in ret [0 1] deposit-date)
        ret (assoc-in ret [(-> ret count dec) 2] value-date)]
    ret))

(defn- find-interest-rate-for [interest-rates-intervals d]
  (if (in-interval? (first interest-rates-intervals) d)
    (ffirst interest-rates-intervals)
    (recur (rest interest-rates-intervals) d)))

(defn split-interest-rates [interest-rates deposit-date value-date]
  (let [interest-rates (into  [[0 (new java.util.Date 0)]] interest-rates)
        interest-rates (conj interest-rates [0 (new java.util.Date Long/MAX_VALUE)])
        interest-rates-intervals (partition 2 1 interest-rates)
        interest-rates-intervals (map (juxt ffirst (comp second first) (comp second second)) interest-rates-intervals)]
    (if (= deposit-date value-date)
      [[(find-interest-rate-for interest-rates-intervals deposit-date) deposit-date value-date]]
      (split-interest-rate-intervals interest-rates-intervals deposit-date value-date))))

(defn compute-face-value [amount interest-rates deposit-date value-date]
  (loop [interest-rates (split-interest-rates interest-rates deposit-date value-date)
         amount amount]
    (let [[interest-rate start end] (first interest-rates)
          amount (compute-simple-face-value amount interest-rate start end)]
      (if (empty? (rest interest-rates))
        (long amount)
        (recur (rest interest-rates) amount)))))

(defn get-interest-rate-history [db sec-id]
  (let [history (d/history db)
        interest-rates (d/q '[:find ?interest-rate ?v
                              :in $ ?eid
                              :where [?e :skirmish.security/interest-rate ?interest-rate ?tx]
                                     [?e :skirmish.security/sec-id ?sec-id]
                                     [?tx :db/txInstant ?v]]
                            history sec-id)]
    (sort-by second interest-rates)))

(defn compute-face-value-from-db [db amount sec-id date]
  (compute-face-value amount
                      (get-interest-rate-history db sec-id)
                      date
                      (new java.util.Date)))

(defn get-amount-in-account [db account-id]
  (let [[amount sec-id pk date] (d/q '[:find [?amount ?sec-id ?pk ?v]
                                       :in $ ?account-id
                                       :where [?e :skirmish.ledger/owner-public-key ?account-id]
                                              [?e :skirmish.ledger/amount ?amount ?tx]
                                              [?e :skirmish.ledger/sec ?sec]
                                              [?sec :skirmish.security/sec-id ?sec-id]
                                              [?e :skirmish.ledger/owner-public-key ?pk]
                                              [?tx :db/txInstant ?v]]
                                     db account-id)]
    {:amount  (when (some? sec-id) (compute-face-value-from-db db amount sec-id date))
     :sec-id sec-id
     :account-public-key pk}))

(defn get-all-securities-id [db]
  (d/q '[:find ?s :where [?e :skirmish.security/sec-id ?s]] db))

(defn create-security [conn sec-id interest-rate]
  (d/transact conn [[:skirmish.ledger/create-security sec-id interest-rate]]))

(defn get-account [db account-id]
  (d/pull db '[*] [:skirmish.ledger/owner-public-key account-id]))

(defn assoc-accounts [db transaction]
  (let [from-account (get-account db (:from-account-id transaction))
        to-account (get-account db (:to-account-id transaction))
        ret (assoc transaction :from-account from-account)
        ret (assoc ret :to-account to-account)]
    ret))

(defn get-new-from-account-value [{amount :amount from-account :from-account} old-change]
  (if (some? old-change)
    (assoc old-change 3 (- (get old-change 3) amount))
    [:db/add
     (:db/id from-account)
     :skirmish.ledger/amount
     (- (:skirmish.ledger/amount from-account) amount)]))

(defn get-new-to-account-value [{amount :amount to-account :to-account} old-change]
  (if (some? old-change)
    (assoc old-change 3 (+ (get old-change 3) amount))
    [:db/add
     (:db/id to-account)
     :skirmish.ledger/amount
     (+ (:skirmish.ledger/amount to-account) amount)]))

(defn compute-transactions [res transaction]
  (let [res-from-account (get res (:from-account-id transaction))
        from-account (get-new-from-account-value transaction res-from-account)
        res (assoc res (:from-account-id transaction) from-account)
        res-to-account (get res (:to-account-id transaction))
        to-account (get-new-to-account-value transaction res-to-account)
        res (assoc res (:to-account-id transaction) to-account)]
    res))

(defn validate-transactions [transactions]
  (loop [transactions transactions]
    (when (seq transactions)
      (let [transaction (first transactions)]
        (when (not= (-> transaction :from-account :skirmish.ledger/sec)
                    (-> transaction :to-account :skirmish.ledger/sec))
          (throw+ {:error-type :4XX
                   :status 400
                   :body {:error "Part of the transaction includes a transaction between two wallets of different underlying securities."}}))
        (recur (rest transactions)))))
  transactions)

(defn validate-resulting-transactions [transactions]
  (loop [transactions transactions]
    (when (seq transactions)
      (let [transaction (first  transactions)]
        (when (neg? (get transaction 3))
          (throw+ {:error-type :4XX
                   :status 400
                   :body {:error "Transaction would result in a negative amount in a wallet."}})))
      (recur (rest transactions))))
  transactions)

(defn create-transactions [db transactions]
  (let [transactions (map #(assoc-accounts db %) transactions)
        transactions (validate-transactions transactions)
        ret (vals (reduce compute-transactions {} transactions))]
    (validate-resulting-transactions ret)))