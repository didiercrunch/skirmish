(ns skirmish.ledger.accounts
  (:require [datomic.api :as d]
            [slingshot.slingshot :refer [throw+]]))

(def db-uri "datomic:free://localhost:4334/skirmish")

(def create-securities
  [
   {:db/ident :skirmish.ledger/create-security
    :db/doc   "Create a security in the system."
    :db/fn    (d/function {:lang   "clojure"
                           :requires ['[slingshot.slingshot]]
                           :params '[db sec-id interest-rate]
                           :code   '(let [security (d/pull db '[:db/id] [:skirmish.security/sec-id sec-id])]
                                      (when (-> security :db/id some?)
                                        (slingshot.slingshot/throw+ {:error-type :4XX :status 400 :body {:error "The security already exists."}}))
                                      [#:skirmish.security{:sec-id sec-id :interest-rate interest-rate}])})}])

(def print-securities
  [
   {:db/ident :skirmish.ledger/emit-securities
    :db/doc   "Print securities fresh from the press."
    :db/fn    (d/function {:lang     "clojure"
                           :requires ['[slingshot.slingshot]]
                           :params   '[db sec-id amount account-public-key]
                           :code     '(let [security (d/pull db '[:db/id] [:skirmish.security/sec-id sec-id])
                                            account (d/pull db '[:skirmish.ledger/amount :skirmish.security/sec] [:skirmish.ledger/owner-public-key account-public-key])
                                            base-amount (get account :skirmish.ledger/amount 0)
                                            entity (:db/id account "new-account")]
                                        (when (-> security :db/id nil?)
                                          (slingshot.slingshot/throw+ {:error-type :4XX :status 404 :body {:error "No security with such id."}}))
                                        (when (and (not= (:skirmish.security/sec account) (:db/id security)) (some? (:db/id account)))
                                          (slingshot.slingshot/throw+ {:error-type :4XX :status 400 :body {:error "The security emitted does not match the account security."}}))
                                        [[:db/add entity :skirmish.ledger/amount (+ base-amount amount)]
                                         [:db/add entity :skirmish.ledger/owner-public-key account-public-key]
                                         [:db/add entity :skirmish.ledger/sec (:db/id security)]])})}])

(def create-transactions
  [{:db/ident :skirmish.ledger/create-transactions
    :db/doc   "Move money from one account to the other."
    :db/fn    (d/function {:lang     "clojure"
                           :requires ['[slingshot.slingshot]]
                           :params   '[db transactions]
                           :code     '(let [from-account-id (map :from-account-id transactions)
                                            from-accounts (d/pull-many db '[*] (for [] [:skirmish.ledger/owner-public-key "2-sec-2"]))]
                                        [])})}])

;; there are different types of securities...
;; 1. currencies, which pays interest rates in their one sec-id.
;; 2. equities, which do not pay interest rates.
;; 3. bonds, which pays interest rates in another currency.

(def schema-securities
  [
   {
    :db/ident :skirmish.security/sec-id
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity
    :db/doc "The security id."}
   {
    :db/ident :skirmish.security/name
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The usual name of the security."}
   {
    :db/ident :skirmish.security/type
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one
    :db/doc "The type of security.  For instance :equity or :currency."}
   {
    :db/ident :skirmish.security/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The description of the currency.  Please be verbose."}
   {
    :db/ident :skirmish.security/emitter
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The public key of the emmiter.  This is not yet supported."}
   {
    :db/ident :skirmish.security/number-of-decimal
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The number of decimal to show in the pretty printing."}
   {
    :db/ident :skirmish.security/symbol
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The symbol of the security for pretty printing."}
   {
    :db/ident :skirmish.security/interest-rate
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The daily interest-rate payed by the security as a multiple of 0.01% (100 is 1% interest rate)."}
   {
    :db/ident :skirmish.security/inflation
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The daily inflation as a multiple of 0.01% (100 is 1%)."}])

(def ledger-schema
  [
   {
    :db/ident :skirmish.ledger/owner-public-key
    :db/unique :db.unique/identity
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The public key of the owner"}
   {
    :db/ident :skirmish.ledger/amount
    :db/index false
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The quantity of currency owned by this account."}
   {
    :db/ident :skirmish.ledger/sec
    :db/index false
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one
    :db/doc "The sec kept in this account."}])

(def schema (-> []
                (concat schema-securities)
                (concat ledger-schema)
                (concat create-securities)
                (concat print-securities)
                (concat create-transactions)))


(defn add-default-currencies [conn]
  (let [dapm #:skirmish.security{:sec-id :dapm
                                 :name "didier amyot paper money"
                                 :type :currency
                                 :description "The official currency of the 'Skirmish Shitty Exchange'."
                                 :emitter "dapm private key"
                                 :number-of-decimal 2
                                 :symbol "რ"
                                 :interest-rate (* 100 10)
                                 :inflation (* 100 9)}
        apbm #:skirmish.security{:sec-id :apbm
                                 :name "apache beam"
                                 :type :equity
                                 :description "Unified programming model to define and execute data processing pipelines, including ETL, batch and stream (continuous) processing."
                                 :emitter "apbm private key"
                                 :number-of-decimal 0}
        apac #:skirmish.security{:sec-id :apac
                                 :name "apache accumulo"
                                 :type :equity
                                 :description "A sorted, distributed key/value store that provides robust, scalable data storage and retrieval."
                                 :emitter "apac private key"
                                 :number-of-decimal 0}
        apax #:skirmish.security{:sec-id :apax
                                 :name "apache apex"
                                 :type :equity
                                 :description "Enterprise-grade unified stream and batch processing engine."
                                 :emitter ":apax private key"
                                 :number-of-decimal 0}
        apdr #:skirmish.security{:sec-id :apdr
                                 :name "apache drill"
                                 :type :equity
                                 :description "Schema-free SQL Query Engine for Hadoop, NoSQL and Cloud Storage."
                                 :emitter ":apdr private key"
                                 :number-of-decimal 0}
        apmh #:skirmish.security{:sec-id :apmh
                                 :name "apache mahout"
                                 :type :equity
                                 :description "For Creating Scalable Performant Machine Learning Applications."
                                 :emitter ":apmh private key"
                                 :number-of-decimal 0}]
    @(d/transact conn [dapm apbm apac apax apdr apmh])))

(defn create-connection
  ([uri init-when-new?]
   (let [new-db? (d/create-database uri)
         conn (d/connect uri)]
     @(d/transact conn schema)
     (if (and init-when-new? new-db?)
       (add-default-currencies conn))
     conn))
  ([uri]
   (create-connection uri true)))

