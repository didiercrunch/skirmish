(ns skirmish.ledger.controllers
  (:require [skirmish.rest.util :refer [build-response get-body cors-middleware]]
            [skirmish.ledger.data-layer :as data-layer]
            [skirmish.routes.core :as routes]
            [compojure.core :as compojure]
            [compojure.route :refer [not-found]]
            [clojure.spec.alpha :as s]
            [datomic.api :as d]
            [slingshot.slingshot :refer [try+ throw+]]))



(defn get-api-description [request]
  (let [body  [{:url "ledger/accounts" :name "accounts"}
               {:url "ledger/securities" :name "securities"}]]
    (build-response request body)))

(defn get-available-accounts-body [accounts]
  (let [sec-id->url #(routes/get-path-for :ledger/account {:account-id (name %)})
        to-security-description #(hash-map :account-public-key % :url (sec-id->url %))
        xf (comp cat (map to-security-description))
        securities-info  (sequence xf accounts)]
    {:accounts securities-info}))


(defn get-available-accounts [conn]
  (fn [request]
    (let [db (d/db conn)
          resp (get-available-accounts-body (data-layer/get-all-account-public-key db))]
      (build-response request resp))))

(defn get-account-body [request {sec-id :sec-id amount :amount account-public-key :account-public-key}]
  (if (nil? sec-id)
    (build-response request {:status 404})
    (let [sec-url (str "/api/1/ledger/securities/" (name sec-id))
          beautify-account { :amount amount
                            :sec-id sec-id
                            :account-public-key account-public-key}
          ret {:account beautify-account  :sec-url sec-url}]
      (build-response request ret))))

(defn get-account [conn]
  "Returns the value of an account."
  (fn [{{account-id :account-id} :route-params :as request}]
    (let [db (d/db conn)
          account-info (data-layer/get-amount-in-account db account-id)]
      (get-account-body request account-info))))

(defn get-account [conn]
  "Returns the value of an account."
  (fn [{{account-id :account-id} :route-params :as request}]
    (let [db (d/db conn)
          account-info (data-layer/get-amount-in-account db account-id)]
      (get-account-body request account-info))))

(defn get-transactions [conn]
  (fn [request]
    (let [all-pending-transactions []]
      (build-response request all-pending-transactions))))

(defn create-transactions [conn]
  (fn [{body :body :as request}]
    (try+
      (build-response request {:message "merci beaucoup"})
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))

(defn get-available-securities-body [securities]
  (let [sec-id->url #(routes/get-path-for :ledger/security {:sec-id (name %)})
        to-security-description #(hash-map :sec-id % :url (sec-id->url %))
        xf (comp cat (map to-security-description))
        securities-info  (sequence xf securities)];;
    {:securities securities-info}))


(defn get-available-securities [conn]
  (fn [request]
    (let [db (d/db conn)
          securities (data-layer/get-all-securities-id db)]
      (build-response request (get-available-securities-body securities)))))

(defn get-new-security [raw-securities]
  (if (s/valid? :skirmish.ledger.core/security raw-securities)
    raw-securities
    (throw+ {:error-type :4XX :status 400 :body {:error "Invalid security format."}})))

(defn create-security [conn]
  "Create a fresh new security."
  (fn [{body :body :as request}]
    (try+
      (let [[sec-id interest-rate] (-> body get-body get-new-security)
            new-sec @(data-layer/create-security conn sec-id interest-rate)]
        (build-response request {:body {:success true}
                                 :headers {"location" (str "/api/1" "/ledger/securities/" (name sec-id))}}))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))


(defn prettify-security [e db]
  (let [{interest-rate :skirmish.security/interest-rate
         description :skirmish.security/description
         id :db/id
         sec-id :skirmish.security/sec-id
         symbol :skirmish.security/symbol
         name :skirmish.security/name
         number-of-decimal :skirmish.security/number-of-decimal
         type_ :skirmish.security/type} e
         interest-rate-history (data-layer/get-interest-rate-history db id)
         volumes (d/q '[:find ?amount ?date
                        :in $ ?id
                        :where [?e :skirmish.ledger/sec ?id]
                               [?e :skirmish.ledger/amount ?amount ?tx]
                               [?tx :db/txInstant ?date]]
                       db id)
         now (new java.util.Date)
         volume (reduce + 0 (map #(data-layer/compute-face-value (first %) interest-rate-history (second %) now) volumes))]
    {:sec-id sec-id
     :interest-rate interest-rate
     :volume (or volume 0)
     :name name
     :interest-rate-history interest-rate-history
     :symbol symbol
     :number-of-decimal number-of-decimal
     :type type_
     :description description}))

(defn get-security-from-db [conn sec-id]
  (let [db (d/db conn)
        e (d/pull db '[*] [:skirmish.security/sec-id sec-id])]
    (if (-> e :db/id nil?)
      (throw+ {:error-type :4XX :status 404 :body {:error "Security not found."}})
      (prettify-security e db))))

(defn get-security [conn]
  "Returns the security."
  (fn [{{sec-id-str :sec-id} :route-params  :as request}]
    (try+
      (let [sec-id (keyword sec-id-str)]
        (build-response request (get-security-from-db conn sec-id)))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))

(defn update-security [conn]
  "Update a security with new features."
  (fn [request]
    (try+
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))

(defn get-emit-securities-request [[amount account] sec-id]
  (if (s/valid? :skirmish.ledger.core/emit-securities [sec-id amount account])
    [:skirmish.ledger/emit-securities sec-id amount account]
    (throw+ {:error-type :4XX :status 400 :body {:error "Invalid emit request."}})))


(defn emit-securities [conn]
  "Emit securities in the specified wallet.  This is the only way to
  create securities.

  payload
    [:amount :account]"
  (fn [{{sec-id-str :sec-id} :route-params body :body :as request}]
    (try+
      (let [emit-request (-> body get-body (get-emit-securities-request (keyword sec-id-str)))]
        @(d/transact conn [emit-request])
        (build-response request {}))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))

(defn create-routes-for-ledger [conn]
  (compojure/routes
    (routes/context :ledger []
                    (routes/GET :ledger/home [] get-api-description)
                    (routes/GET :ledger/accounts [] (get-available-accounts conn))
                    (routes/GET :ledger/account [] (get-account conn))
                    (routes/GET :ledger/transactions [] (get-transactions conn))
                    (routes/POST :ledger/transactions [] (create-transactions conn))
                    (routes/GET :ledger/securities [] (get-available-securities conn))
                    (routes/POST :ledger/securities [] (create-security conn))
                    (routes/GET :ledger/security [] (get-security conn))
                    (routes/PUT :ledger/security [] (update-security conn))   ;; TODO
                    (routes/POST :ledger/security [] (emit-securities conn)))
    (not-found "not found")))