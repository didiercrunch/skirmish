(ns skirmish.ledger.core
  (:require [org.httpkit.server :refer [run-server]]
            [skirmish.ledger.controllers :as controllers]
            [compojure.handler :refer [api]]
            [skirmish.rest.util :refer [cors-middleware]]
            [skirmish.ledger.accounts]
            [taoensso.timbre :as timbre]
            [clojure.spec.alpha :as s]))

(s/def ::transaction (s/cat :type #{:transfer-immediately}
                            :from-account-id string?
                            :to-account-id string?
                            :amount int?))

(s/def ::transactions (s/coll-of ::transaction))

(s/def ::security (s/cat :sec-id keyword?
                         :interest-rate nat-int?
                         :type #{:equity :currency}
                         :description string?
                         :emitter string?
                         :symbol (s/? string?)))

(s/def ::emit-securities (s/cat :sec-id keyword?
                                :amount nat-int?
                                :account string?))

(defn -main [config securities-api exchange-apis]
  (let [ledger-config (:ledger-server config)
        conn (skirmish.ledger.accounts/create-connection (-> config :datomic :transactor-url))
        api-routes (controllers/create-routes-for-ledger conn)]
    (timbre/info "Running skirmish.ledger.core server with config: " ledger-config)
    (run-server
      (-> api-routes api cors-middleware)
      ledger-config)))

