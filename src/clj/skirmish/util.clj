(ns skirmish.util
  (:require [clojure.core.async :as async])
  (:import [ skirmish PersistentSortedList]))

(defn sorted-list [& keys]
  (PersistentSortedList/create (seq keys)))

(defn sorted-list-by [comparator & keys]
  (PersistentSortedList/create comparator (seq keys)))

(defn block-thread []
  "Block the current thread until chuck norris gets weaker."
  (async/<!! (async/chan)))
