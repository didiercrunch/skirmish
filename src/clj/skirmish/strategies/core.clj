(ns skirmish.strategies.core
  (:require [clojure.core.async :as async :refer [>! <!]]))


(defn strategy-one [])

(defprotocol SimpleStrategy
  "Implement this protocol if you wish to make a strategy"
  (get-urls-to-fetch [strategy msecs]
    "returns a list of all the url to fetch.")
  (get-wished-trades [strategy msec data]
    ""))

(defrecord TheBananaStrategy [sec-id look-back-in-days period])

(extend-type TheBananaStrategy
  SimpleStrategy
  (get-urls-to-fetch [strat msecs]
    (let [sec-id (-> strat :sec-id name)
          one-day (* 24 3600 1000)
          from (- msecs (-> strat :look-back-in-days (* one-day)))
          period (:period strat)
          to msecs]
      (format "/api/v1/securities/%s/l1?from=%s&to=%s&period=period" sec-id from to period))))


(defn get-data-for-strategy [strategy])


(defn make-trades [wanted-trades])


(defn framework [strategy msecs]
  (async/go-loop [_ (<! (async/timeout msecs))]
    (let [data-for-strategy (<! (get-data-for-strategy strategy))
          wanted-trades (strategy data-for-strategy)
          results (<! (make-trades wanted-trades))])
    (recur (<! (async/timeout msecs)))))

