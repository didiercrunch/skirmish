(ns skirmish.broker.db
  (:require [datomic.api :as d]
            [slingshot.slingshot :refer [throw+]]))

(def create-user
  [
   {:db/ident :skirmish.broker.user/create-user
    :db/doc   "Create a new user in the system."
    :db/fn    (d/function {:lang   "clojure"
                           :requires ['[slingshot.slingshot]]
                           :params '[db email accounts]
                           :code   '(let [existing-user (d/pull db '[:db/id] [:skirmish.broker.user/email email])]
                                      (when (-> existing-user :db/id some?)
                                        (slingshot.slingshot/throw+ {:error-type :4XX :status 400 :body {:error "A user with this email already exists."}}))
                                      [#:skirmish.broker.user{:email email :accounts accounts}])})}])


(def accounts-schema [
                      {
                       :db/ident :skirmish.broker.account/id
                       :db/unique :db.unique/identity
                       :db/valueType :db.type/uuid
                       :db/cardinality :db.cardinality/one
                       :db/doc "The id of an account."}
                      {
                       :db/ident :skirmish.broker.account/sec-id
                       :db/valueType :db.type/keyword
                       :db/cardinality :db.cardinality/one
                       :db/doc "The security id of the account."}
                      {
                       :db/ident :skirmish.broker.account/private-key
                       :db/valueType :db.type/string
                       :db/cardinality :db.cardinality/one
                       :db/doc "The private key of the account."}])


(def user-schema [
                  {
                   :db/ident :skirmish.broker.user/email
                   :db/unique :db.unique/identity
                   :db/valueType :db.type/string
                   :db/cardinality :db.cardinality/one
                   :db/doc "The email of the user; used as id."}
                  {
                   :db/ident :skirmish.broker.user/accounts
                   :db/valueType :db.type/ref
                   :db/cardinality :db.cardinality/many
                   :db/doc "The list of the accounts own by the user."}])


(def schema (-> []
                (concat user-schema)
                (concat create-user)
                (concat accounts-schema)))

(defn create-connection [uri]
  (let [_ (d/create-database uri)
        conn (d/connect uri)]
    @(d/transact conn schema)
    conn))

