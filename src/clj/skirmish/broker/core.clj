; The backend of the skirmish web user interface.

(ns skirmish.broker.core
  (:require [org.httpkit.server :refer [run-server]]
            [compojure.handler :refer [api]]
            [skirmish.broker.broker :refer [create-routes]]
            [taoensso.timbre :as timbre]
            [skirmish.middleware.core :refer [create-map-exchange-apis]]
            [skirmish.broker.db :refer [create-connection]]
            [skirmish.rest.util :refer [cors-middleware]]))


(defn -main [config securities-api exchange-apis]
  (let [webserver-config (:api-broker config)
        conn (create-connection (-> webserver-config :datomic :transactor-url))
        api-routes (create-routes conn webserver-config)]
    (timbre/info "Running skirmish.broker.core server with config: " webserver-config)
    (run-server
      (-> api-routes api cors-middleware)
      webserver-config)))
