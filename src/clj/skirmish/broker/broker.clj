(ns skirmish.broker.broker
  (:require [compojure.core :refer :all]
            [compojure.route :refer [not-found]]
            [datomic.api :as d]
            [slingshot.slingshot :refer [try+ throw+]]
            [skirmish.rest.util :refer [build-response not-found-resp get-body]]
            [taoensso.timbre :as timbre]
            [skirmish.routes.core :as routes]
            [clj-http.client :as http-client]))

(defn generate-amount-for-new-user [seed]
  (let [random  (new java.util.Random seed)
        random-fn #(. random nextInt 20000)]
    (* 100 (reduce + (take 100 (repeatedly random-fn))))))

(defn create-account [account-id sec-id]
  #:skirmish.broker.account{:id account-id
                            :sec-id sec-id
                            :private-key (str account-id)})

(defn create-user-account [seed default-sec-id]
  (let [url (routes/get-path-for :ledger/security {:sec-id (name default-sec-id)})
        account-id (java.util.UUID/randomUUID)
        amount (generate-amount-for-new-user seed)
        payload [amount (str account-id)]]
    (timbre/info "Creating new account " account-id " with ammount " amount " of " default-sec-id)
    (timbre/info url)
    (http-client/post url {:body (str payload)})
    (create-account account-id default-sec-id)))

(defn create-user-seed-from-email [email]
  (let [hasher (java.security.MessageDigest/getInstance "SHA-256")
        salt (. "this should be private and set in config" getBytes "utf-8")
        msg (. email getBytes "utf-8")
        _ (doto hasher (.update salt) (.update msg))
        hash (. hasher digest)]
    (int (mod (new BigInteger 1 hash) (biginteger Integer/MAX_VALUE)))))


(defn create-user [conn default-sec-id]
  (fn [{body :body :as request}]
    (try+
       (let [email (-> body get-body :email)
             _ (timbre/info "creating new user" email)
             seed (create-user-seed-from-email email)
             account (create-user-account seed default-sec-id)]
         @(d/transact conn [[:skirmish.broker.user/create-user email [account]]])
         (build-response request {:status 201 :headers {"location" (str "/api/1/users/" email)}}))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))


(defn add-account-urls [user]
  (let [accounts (:skirmish.broker.user/accounts user)
        url #(->> % :skirmish.broker.account/id (str "http://localhost:1604/api/1/ledger/accounts/"))
        ret (map #(assoc % :skirmish.broker.account/url (url %)) accounts)]
    (assoc user :skirmish.broker.user/accounts ret)))


(defn get-user [conn]
  (fn [{{email :email} :route-params :as request}]
    (try+
      (timbre/info "getting user with email" email)
      (let [db (d/db conn)
            users (d/q '[:find (pull ?e [:skirmish.broker.user/email
                                         {:skirmish.broker.user/accounts [:skirmish.broker.account/id
                                                                          :skirmish.broker.account/sec-id]}]) .
                         :in $ ?email
                         :where [?e :skirmish.broker.user/email ?email]] db email)]
        (if (nil? users)
          (build-response request {:status 404 :body {:error (str "cannot find user with email " email)}})
          (build-response request (add-account-urls users))))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))


(defn get-all-users [conn]
  (fn [{{email :email} :route-params :as request}]
    (try+
      (timbre/info "getting user with email" email)
      (let [db (d/db conn)
            emails (d/q '[:find ?email
                          :where [_ :skirmish.broker.user/email ?email]] db)
            xf (comp cat (map #(hash-map :email % :url (str "/api/1/users/" %))))]
        (build-response request
                        {:users (doall (sequence xf emails))}))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))


(defn get-account [conn]
  (fn [email account]))


(defn create-routes [conn config]
  (routes
    (context "/api/1" []
      (POST "/users" [] (create-user conn (:default-sec-id config)))
      (GET "/users" [] (get-all-users conn))
      (GET "/users/:email" [email] (get-user conn))
      (GET "/users/:email/accounts/:account" [email account] (get-account conn)))
    not-found-resp))
