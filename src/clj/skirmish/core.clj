(ns skirmish.core
  (:require [skirmish.exchange.core]
            [skirmish.wui.server]
            [skirmish.bookkeeper.core]
            [skirmish.exchange-gateway.core]
            [skirmish.ledger.core]
            [taoensso.timbre :as timbre]
            [skirmish.broker.core]
            [skirmish.middleware.core :refer [create-exchange-apis-for
                                              create-securities-api-for]]
            [aero.core :refer [read-config]]
            [orchestra.spec.test :as st]
            [clojure.core.async :as async])
  (:gen-class))

(def targets ;; a target is a function that blocks the thread and takes two
             ;; arguments:  the config map and the "exchange-api"
  {:api-server     skirmish.exchange-gateway.core/-main
   :exchange-core  skirmish.exchange.core/-main ;; should be skirmish.exchange.core/api-main
   :api-bookkeeper skirmish.bookkeeper.core/-main
   :ledger         skirmish.ledger.core/-main
   :broker         skirmish.broker.core/-main
   :wui            skirmish.wui.server/-main})

(defn run-all-targets-in-separate-threads [target-fns config securities-api exchange-apis]
  (timbre/warn "running  multiple targets. This is okay in dev but horrible in production")
  (timbre/info (clojure.string/join "\n" target-fns))
  (let [threads (map #(async/thread (% config securities-api exchange-apis)) target-fns)
        finished-threads (async/merge threads)]
    (loop [finished-thread (async/<!! finished-threads)]
      (when (some? finished-thread)
          (recur (async/<!! finished-threads))))))

(defn run-all-targets [target-fns config]
  (let [securities-api (create-securities-api-for config)
        exchange-apis (create-exchange-apis-for config securities-api)]
    (if (= (count target-fns) 1)
      ((first target-fns) config securities-api exchange-apis)
      (run-all-targets-in-separate-threads target-fns config securities-api exchange-apis))
    (skirmish.util/block-thread)))

(defn -main [& argv]
  (let [config (-> "config.edn" clojure.java.io/resource read-config)
        xf (comp (map keyword) (map targets) (filter some?))
        target-fns (sequence xf argv)]
    (if (true? (:instrument-spec config))
      (st/instrument))
    (if (seq target-fns)
      (run-all-targets target-fns config)
      (do
        (timbre/warn argv)
        (timbre/warn "target" target-fns "not found")
        (timbre/warn "all known targets are...")
        (timbre/warn (keys targets))))))