(ns skirmish.exchange-gateway.core
  (:require [org.httpkit.server :refer [run-server]]
            [compojure.handler :refer [api]]
            [skirmish.util :refer [block-thread]]
            [skirmish.exchange-gateway.exchange-gateway :refer [create-routes-for-exchange]]
            [skirmish.middleware.core :refer [create-map-exchange-apis]]
            [taoensso.timbre :as timbre]
            [ring.middleware.cors :refer [wrap-cors]]))


(defn -main [config securities-api exchange-apis]
  (let [webserver-config (:api-server config)
        api-routes (-> exchange-apis create-map-exchange-apis create-routes-for-exchange)]
    (timbre/info "Running skirmish.exchange-gateway.core server with config:" webserver-config)
    (run-server (api api-routes) webserver-config)))


