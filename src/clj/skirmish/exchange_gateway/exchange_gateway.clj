(ns skirmish.exchange-gateway.exchange-gateway
  (:require [compojure.route :refer [not-found]]
            [compojure.core :refer :all]
            [skirmish.order]  ;; for spec side effect
            [skirmish.rest.util :refer [build-response get-body]]
            [clojure.spec.alpha :as s]
            [kinsky.client]
            [slingshot.slingshot :refer [try+ throw+]]
            [clojure.core.async :refer [>!!]]))


(def order-id (atom -1))


(defn validate-order [asked-order sec-id]
  (let [now (System/currentTimeMillis)]
    (if (not (s/valid? :book.order/order asked-order))
      (throw+ {:error-type :4XX
               :status 400
               :body {:error "Invalid order."
                      :spec (s/explain-data :book.order/order asked-order)}}))
    (into asked-order {:book.order/id (swap! order-id inc)
                       :book.order/sec-id sec-id
                       :book.order/entry-timestamp now})))


(defn get-exchange-api [sec-id exchange-apis]
  (if (contains? exchange-apis sec-id)
    (get exchange-apis sec-id)
    (throw+ {:error-type :4XX :status 404 :body {:error "Security not available."}})))


(defn create-order [exchange-apis]
  (fn [{{sec-id-str :sec-id} :route-params body :body :as request}]
    (try+
      (let [asked-order (get-body body)
            exchange-api (get-exchange-api (keyword sec-id-str) @exchange-apis)
            incoming-order-channel (:new-order-submitter exchange-api)
            order (validate-order asked-order (:sec-id exchange-api))]
        (>!! incoming-order-channel order)
        (build-response request order))
      (catch [:error-type :4XX] e (build-response request e))
      (catch [:error-type :5XX] e (build-response request e)))))


(defn create-routes-for-exchange [exchange-apis]
  (routes
    (context "/api/1" []
      (POST "/securities/:sec-id" [] (create-order exchange-apis)))
    (not-found "not found")))

