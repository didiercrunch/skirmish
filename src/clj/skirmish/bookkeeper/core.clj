; This is the service that broadcast market data to the
; world.  The users need to expect websocket and REST api.


(ns skirmish.bookkeeper.core
  (:require [org.httpkit.server :refer [run-server]]
            [compojure.handler :refer [api]]
            [taoensso.timbre :as timbre]
            [skirmish.middleware.core :refer [create-map-exchange-apis]]
            [skirmish.bookkeeper.bookkeeper :refer [create-routes-for-bookkeeper]]
            [skirmish.rest.util :refer [cors-middleware]]))

(defn -main [config securities-api exchange-apis-mult-chan]
  (let [bookkeeper-config (:api-bookkeeper config)
        exchange-apis (create-map-exchange-apis exchange-apis-mult-chan)
        api-routes (create-routes-for-bookkeeper exchange-apis config)]
    (timbre/info "Running skirmish.bookkeeper.core server with config: " bookkeeper-config)
    (run-server
      (-> api-routes api cors-middleware)
      bookkeeper-config)))
