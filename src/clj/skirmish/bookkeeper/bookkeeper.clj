(ns skirmish.bookkeeper.bookkeeper
  (:require [skirmish.exchange.exchange :refer [empty-book]]
            [org.httpkit.server :refer [run-server with-channel send! close on-close]]
            [compojure.route :refer [not-found]]
            [compojure.handler :refer [api]]
            [compojure.core :refer :all]
            [skirmish.order]  ;; for spec side effect
            [skirmish.rest.util :refer [build-response build-response-body]]
            [clojure.spec.alpha :as s]
            [slingshot.slingshot :refer [try+ throw+]]
            [clojure.core.async :refer [chan go-loop close! <! sub]]))


(def order-id (atom -1))

(defn endpoint [name description path]
  {:name name :description description :url path})

(def endpoints [(endpoint "securities" "list all securities." "/api/1/securities")])


(defn api-description [req]
  (let [body endpoints]
    (build-response req body)))

(defn subscribe-to-orders-publisher [exchange-api status-to-listen-on]
  (let [ret (chan)
        new-book-order-channel (:orders-publisher exchange-api)]
    (doseq [status status-to-listen-on]
      (sub new-book-order-channel status ret))
    ret))


(defn get-new-order-channel [exchange-apis sec-id status-to-listen-on]
  (let [exchange (get exchange-apis sec-id)]
    (subscribe-to-orders-publisher exchange status-to-listen-on)))


(defn get-new-trades-channel [exchange-apis sec-id]
  (let [exchange (get exchange-apis sec-id)
        new-trades-channel (:trades-publisher exchange)
        ret (chan)]
    (sub new-trades-channel :sell ret)
    (sub new-trades-channel :buy ret)
    ret))


(defn securities [exchange-apis config]
  (fn [request]
    (build-response request {:securities (:securities config)})))


(defn get-security [exchange-apis]
  (fn [{{sec-id-str :sec-id} :route-params body :body :as request}]
    (build-response request {:security {:sec-id sec-id-str}})))


(defn chan-to-websocket [request input-chan ws-chan]
  (on-close ws-chan (fn [status] (close! input-chan)))
  (go-loop []
    (when-let [e (<! input-chan)]
      (send! ws-chan (build-response-body request e))
      (recur))))


(defn get-status-to-listen-on [{params :query-params}]
  (let [param-status (:status params)
        status (if (seq? param-status) param-status skirmish.order/order-status)]
    (filter #(s/valid? :book.order/status %) status)))


(defn listen-to-orders [exchange-apis]
  (fn [{{sec-id-str :sec-id} :route-params :as request}]
    (with-channel request ws-chan
                  (let [status-to-listen-on (get-status-to-listen-on request)
                        order-channel (get-new-order-channel @exchange-apis (keyword sec-id-str) status-to-listen-on)]
                    (chan-to-websocket request order-channel ws-chan)))))


(defn listen-to-trades [exchange-apis]
  (fn [{{sec-id-str :sec-id} :route-params :as request}]
    (with-channel request ws-chan
                  (let [order-channel (get-new-trades-channel @exchange-apis (keyword sec-id-str))]
                    (chan-to-websocket request order-channel ws-chan)))))


(defn create-book [exchange-api]
  (let [ret (atom {})
        new-book-order-channel (subscribe-to-orders-publisher exchange-api skirmish.order/order-status)]
    (go-loop [order (<! new-book-order-channel)]
      (let [order-id (:book.order/id order)
            to-remove (contains? #{:withdrawn :totally-fulfilled} (:book.order/status order))]
        (swap! ret (fn [book]
                     (if to-remove
                       (dissoc book order-id)
                       (assoc book order-id order)))))
      (recur (<! new-book-order-channel)))
    ret))

(defn map-values [f m]
  (into {} (for [[k v] m] [k (f v)])))

(defn get-books [exchange-apis books]
  (fn [{{sec-id-str :sec-id} :route-params body :body :as request}]
    (let [sec-id (keyword sec-id-str)]
      (build-response request @(get books sec-id)))))


(defn create-routes-for-bookkeeper [exchange-apis config]
  (let [books (map-values #(create-book %) @exchange-apis)]
    (routes
      (context "/api/1" []
        (GET "/" [] api-description)
        (GET "/securities" [] (securities exchange-apis config))
        (GET "/securities/:sec-id" [] (get-security exchange-apis))
        (GET "/securities/:sec-id/trades" [] (listen-to-trades exchange-apis))
        (GET "/securities/:sec-id/orders" [] (listen-to-orders exchange-apis))
        (GET "/securities/:sec-id/book" [] (get-books exchange-apis books)))
      (not-found "not found"))))
