(ns skirmish.rest.util
  (:require [cheshire.core :refer [generate-string]]
            [slingshot.slingshot :refer [try+ throw+]]
            [clojure.edn :as edn]
            [ring.middleware.cors :refer [wrap-cors]]
            [clojure.string :refer [split starts-with? ends-with?]]))

;; Access-Control-Allow-Origin

(def mime-types
  [["application/json" generate-string]
   ["application/edn" str]])

(defn cors-middleware [handler]
  (wrap-cors handler
             :access-control-allow-origin #"http://localhost:3449"
             :access-control-allow-credentials true
             :access-control-allow-methods [:get :put :post :delete]))


(defn create-mime-type-predicate [accept]
  (let [[accept-start accept-end] (split accept #"/")]
    (fn [mimetype]
      (let [[start end] (split mimetype #"/")]
        (and (or (= "*" accept-start) (= start accept-start))
             (or (= "*" accept-end) (= end accept-end)))))))


(defn get-best-serializer [request]
  (let [accept (get-in request [:headers "accept"] "*/*")
        accept-content-types (conj (split accept #",\s*") "*/*")
        xf (comp (map create-mime-type-predicate)
                 (map (fn [mime-pred] (filter #(mime-pred (first %))  mime-types)))
                 cat
                 (take 1))]
    (first (sequence xf accept-content-types))))


(defn get-status [status request]
  (if (nil? status)
    (case (:request-method request)
      :get 200
      :post 201
      200)
    status))

(defn build-response-body [request body]
  (let [[_ serializer] (get-best-serializer request)]
    (serializer body)))

(defn build-response [request {status :status body :body headers :headers :as response}]
  (let [[content-type serializer] (get-best-serializer request)
        ret-headers (if (some? headers) headers {})]
    {:body (serializer (if (nil? body) response body))
     :headers (into {"Content-Type" content-type} ret-headers)
     :status (get-status status request)}))

(defn not-found-resp [request]
  (build-response request {:status 404 :body {:error "not found"}}))

(defn get-body [body]
  (try+
    (-> body clojure.java.io/reader slurp edn/read-string)
    (catch Exception e
      (throw+ {:error-type :4XX :status 400 :body {:error "Invalid body format.  Body must be edn format."}}))))

(defn string->stream
  ([s] (string->stream s "UTF-8"))
  ([s encoding]
   (-> s
       (.getBytes encoding)
       (java.io.ByteArrayInputStream.))))

(def edn->stream (comp string->stream str))