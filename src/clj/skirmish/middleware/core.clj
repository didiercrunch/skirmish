(ns skirmish.middleware.core
  (:require [clojure.core.async :as async]
            [clojure.core.async :refer [go-loop <! sub chan]]
            [kinsky.client]))

(defn create-chan-based-exchange-api-for [sec-id]
  (let [new-order (async/chan)
        trades (async/chan)
        orders (async/chan)]
    {:sec-id                 sec-id
     :disabled               false
     :new-order-publisher    new-order  ;; anything published here will be processed as
     :new-order-submitter    new-order
     :trades-publisher       (async/pub trades :book.order/type)
     :trades-submitter       trades
     :orders-publisher       (async/pub orders :book.order/status)
     :orders-submitter       orders}))


(defn create-chan-based-security-api [config]
  (let [securities (async/chan)]
    {:securities-publisher (async/mult securities)
     :securities-submitter securities}))

(def security-api-creators {})

(defn create-securities-api-for [config]
  (let [creator (get security-api-creators (:middleware config) create-chan-based-security-api)]
    (creator config)))

(defn update-exchange-apis [create-exchange-api-for sec]
  (let [sec-id (:sec-id sec)
        disabled? (get sec :disabled false)]
    (if disabled?
      {:sec-id sec-id :disabled true}
      (create-exchange-api-for sec-id))))

(def exchange-api-creators {})

(defn create-exchange-apis-for [config securities-api]
  (let [ret (async/chan)
        securities-ch (async/chan)
        securities-publisher (:securities-publisher securities-api)
        create-exchange-api-for (get exchange-api-creators (:middleware config) create-chan-based-exchange-api-for)]
    (async/tap securities-publisher securities-ch)
    (go-loop [sec (<! securities-ch)]
           (async/>! ret (update-exchange-apis create-exchange-api-for sec))
           (recur (<! securities-ch)))
    (async/mult ret)))

(defn create-map-exchange-apis [exchange-apis-mult-chan]
  (let [c (async/chan)
        ret (atom {})]
    (async/tap exchange-apis-mult-chan chan)
    (async/go-loop [exchange-api (async/<! c)]
      (if (:disabled? exchange-api)
        (swap! ret #(dissoc % (:sec-id exchange-api)))
        (swap! ret #(assoc % (:sec-id exchange-api) exchange-api))))
    ret))
