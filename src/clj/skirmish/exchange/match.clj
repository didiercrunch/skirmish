(ns skirmish.exchange.match
  (:require [skirmish.util :refer [sorted-list-by]]
            [clojure.spec.alpha :as s]
            [clojure.edn :as edn]))



;; Let say a book is two collections.  The first called
;; sell-book contains all the sell orders.  The second
;; called buy-book contains all the buy orders.

(defn create-sell-book [& orders]
  "Creates a sell book with the orders in it."
  (let [cmp #(compare (:price %1) (:price %2))]
    (apply sorted-list-by cmp orders)))

(defn create-buy-book [& orders]
  "Creates a buy book with the orders in it."
  (let [cmp #(compare (:price %2) (:price %1))]
    (apply sorted-list-by cmp orders)))

(defn buy? [order]
  (= :buy (:type order)))

(defn sell? [order]
  (= (:type order) :sell))

(defn- sell-order-fullfillable? [book-order order]
  (>= (:price book-order) (:price order)))

(defn- buy-order-fullfillable? [book-order order]
  (<= (:price book-order) (:price order)))

(defn- create-sell-trade-factory [order-to-fulfill]
  (fn [trade book-order]
    (let [already-fulfilled (:book.order/already-fulfilled trade 0)
          qtn-to-fulfill (min (:quantity book-order) (- (:quantity order-to-fulfill) already-fulfilled))
          can-fulfill? (sell-order-fullfillable? book-order order-to-fulfill)]
      (when (and can-fulfill? (< 0 qtn-to-fulfill))
        #:book.order{:type :buy
                     :quantity qtn-to-fulfill
                     :already-fulfilled (+ already-fulfilled qtn-to-fulfill)
                     :price (max (:price book-order) (:price order-to-fulfill))
                     :order-id (:book.order/id order-to-fulfill)
                     :book-id  (:book.order/id book-order)}))))

(defn- create-buy-trade-factory [order-to-fulfill]
  (fn [trade book-order]
    (let [already-fulfilled (:book.order/already-fulfilled trade 0)
          qtn-to-fulfill (min (:quantity book-order) (- (:quantity order-to-fulfill) already-fulfilled))
          can-fulfill? (buy-order-fullfillable? book-order order-to-fulfill)]
      (when (and can-fulfill? (< 0 qtn-to-fulfill))
        #:book.order{:type :sell
                     :quantity qtn-to-fulfill
                     :already-fulfilled (+ already-fulfilled qtn-to-fulfill)
                     :price (min (:price book-order) (:price order-to-fulfill))
                     :order-id (:book.order/id order-to-fulfill)
                     :book-id  (:book.order/id book-order)}))))

(defn- create-trade-factory [order-to-fulfill]
  (if (sell? order-to-fulfill)
    (create-sell-trade-factory order-to-fulfill)
    (create-buy-trade-factory order-to-fulfill)))

(defn- create-trades [order-to-fulfill sell-book]
  (let [create-trade (create-trade-factory order-to-fulfill)
        trades (reductions create-trade {} sell-book)
        ret (rest (take-while some? trades))]
    (map #(dissoc % :book.order/already-fulfilled) ret)))

(defn- partial-trade? [trade order]
  (not= (:quantity order) (:book.order/quantity trade)))

(defn- compute-partial-order [order trade]
  (assoc order :quantity (- (:quantity order) (:book.order/quantity trade 0))))

(defn- drop-in-sorted-list [n sorted-list]
  (cond
    (<= n 0) sorted-list
    (<= (count sorted-list) n) (empty sorted-list)
    :else (drop n sorted-list)));;

(defn- compute-new-book-after-trades [book trades]
  (let [new-book (drop-in-sorted-list (-> trades count dec) book)
        possible-partial-trade (last trades)]
    (if (partial-trade? possible-partial-trade (first new-book))
      (conj (rest new-book) (compute-partial-order (first new-book) possible-partial-trade))
      (rest new-book))))

(defn- leftover-order [order trades]
  (let [traded-quantity (reduce + (map :book.order/quantity trades))
        leftover-quantity  (- (:quantity order) traded-quantity)]
    (when (pos? leftover-quantity)
      (assoc order :quantity leftover-quantity))))

(defn match-order-in-book [book order]
  "Matches the order within the book.  Returns the applied trades,
  the resulting book and left over order if the input order has not
  been entirely fulfilled."
  (let [trades (create-trades order book)
        new-book (compute-new-book-after-trades book trades)
        leftover-order (leftover-order order trades)]
    [trades new-book leftover-order]))

