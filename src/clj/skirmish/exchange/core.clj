(ns skirmish.exchange.core
  (:require [skirmish.util :refer [block-thread]]
            [clojure.core.async :as async]
            [skirmish.middleware.core :refer [create-exchange-apis-for]]
            [skirmish.exchange.exchange :refer [create-exchange-for-security empty-book]]))


(defn create-exchange-for [exchange-api]
  (let [initial-book empty-book
        new-order-publisher (:new-order-publisher exchange-api)
        trades-submitter (:trades-submitter exchange-api)
        orders-submitter (:orders-submitter exchange-api)]

    (create-exchange-for-security initial-book
                                  new-order-publisher
                                  trades-submitter
                                  orders-submitter)))


(defn -main [config securities-api exchange-apis-mult-chan]
  (let [ch (async/chan)]
    (async/tap exchange-apis-mult-chan ch)
    (async/go-loop [exchange-api (async/<! ch)]
      (create-exchange-for exchange-api)))
  (block-thread))