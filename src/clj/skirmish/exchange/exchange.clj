(ns skirmish.exchange.exchange
  (:require [skirmish.exchange.match :refer [sell?
                                             create-buy-book
                                             create-sell-book
                                             match-order-in-book]]
            [skirmish.util :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.core.async :refer [chan go >! <! pub] :as async]))


;; utility functions

(def empty-book {:sell (create-sell-book)
                 :buy (create-buy-book)
                 :id 0})

(defn get-reference-book [order book]
  "The reference book is the book where the order will be fulfilled"
  (if (sell? order)
    (:buy book)
    (:sell book)))

(defn get-same-type-order [order book]
  "the same type order book is the book where the order will be added if not fulfilled"
  (if (sell? order)
    (:sell book)
    (:buy book)))

;;  Bellow are all the functions to match the received order in the correct book
(defn process-abstract [order reference-book same-type-book]
  (let [[trades new-reference-book leftover-order] (match-order-in-book reference-book order)
        new-same-type-book (if leftover-order (conj same-type-book leftover-order) same-type-book)]
    [new-reference-book new-same-type-book leftover-order trades]))


(defn process-sell-order [{sell-book :sell buy-book :buy id :id} order]
  (let [[new-buy-book new-sell-book new-order trades] (process-abstract order buy-book sell-book)
        new-book {:sell new-sell-book :buy new-buy-book :id (inc id)}]
    [new-book new-order trades]))


(defn process-buy-order [{sell-book :sell buy-book :buy id :id} order]
  (let [[new-sell-book new-buy-book new-order trades] (process-abstract order sell-book buy-book)
        new-book {:sell new-sell-book :buy new-buy-book :id (inc id)}]
    [new-book new-order trades]))


(defn process-order [book order]
  (if (sell? order)
    (process-sell-order book order)
    (process-buy-order book order)))

;; Bellow are all the function to transform the order and trades to
;; a list of order

(defn retrieve-new-book-order-from-trade [trade book]
  (let [book-id (:book.order/book-id trade)
        f #(= book-id (:book.order/id %))
        ret-order (first (filter f book))
        new-quantity (- (:quantity ret-order) (:book.order/quantity trade));; (- (:quantity ret-order) (:book.order/quantity trade))
        totally-fulfilled? (= (:book.order/quantity trade) (:quantity ret-order))]
    (into ret-order {:book.order/status         (if totally-fulfilled? :totally-fulfilled :partially-fulfilled)
                     :book.order/status-dual-id (:book.order/order-id trade)
                     :quantity new-quantity})))


(defn get-intermediate-order [order trade]
  (let [new-order-quantity (- (:quantity order) (:book.order/quantity trade))
        totally-fulfilled? (= 0 new-order-quantity)
        dual-id (:book.order/book-id trade)]
    (into order {:book.order/status (if totally-fulfilled? :totally-fulfilled :partially-fulfilled)
                 :book.order/status-dual-id dual-id
                 :quantity new-order-quantity})))


(defn  get-processed-orders-in-trades[order book trades]
  (let [trades-orders (sequence  (map #(retrieve-new-book-order-from-trade % book)) trades)
        processed-orders (rest (reductions get-intermediate-order order trades))]
    (interleave trades-orders processed-orders)))


(defn get-processed-orders [order leftover-order trades book]
  "Computes all the intermediary orders that took place during
  a the trade.
  The internal logic is very complicated.  Probably too complicated."
  (let [ret [(assoc order :book.order/status :received)]
        ret (into ret (get-processed-orders-in-trades order book trades))
        ret (if (seq leftover-order)
              (conj ret (assoc leftover-order :book.order/status :inserted-into-book))
              ret)]
    ret))

(s/fdef get-processed-orders
        :ret (s/* :book.order/order))


;; bellow are the really interesting functions; aka public functions.

(defn create-exchange-for-security [initial-book new-order-publisher trades-submitter orders-submitter]
  (async/go
    (loop [order (<! new-order-publisher) book initial-book]
      (let [[new-book new-order new-trades] (process-order book order)
            orders (get-processed-orders order new-order new-trades (get-reference-book order book))
            orders-copy (async/onto-chan orders-submitter orders false)
            trades-copy (async/onto-chan trades-submitter new-trades false)]
        (<! orders-copy)
        (<! trades-copy)
        (recur (<! new-order-publisher) new-book)))))
