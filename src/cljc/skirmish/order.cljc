(ns skirmish.order
  (:require [clojure.spec.alpha :as s]))

;;  Let say an order is a map of the following keys
;;  :amount  the amount of security to exchange
;;  :price   the price of each security to echange
;;  :type    one of :sell :buy
;;  ::id     maybe an id

(def order-status #{:received  ;; maybe called acknowledged.
                    :partially-matched
                    :totally-matched
                    :inserted-into-book
                    :partially-fulfilled
                    :totally-fulfilled
                    :withdrawn}) ;; for instance the order has been canceled)

(s/def :book.order/type #{:buy :sell})
(s/def :book.order/price nat-int?)
(s/def :book.order/quantity nat-int?)
(s/def :book.order/id    nat-int?)
(s/def :book.order/status order-status)


(s/def :book.order/status-dual-id :book.order/id)
(s/def :book.order/order (s/keys :req-un [:book.order/type :book.order/price :book.order/quantity] :opt [:book.order/id]))

;; A trade is defined by the bellow map...
;;  ::quantity  the amount of exchanged securities
;;  ::price     the price of the security exchanged
;;  ::book-id   the order id of the security traded on the book
;;  ::order-id  the id of the processed ordered
;;  ::type      the type of exchanged made.  :sell means the book
;;              is selling securities.  ::buy means the book is
;;              buying securities.