(ns skirmish.wui.core
    (:require [reagent.core :as reagent :refer [atom]]
              [skirmish.wui.pages :as pages]
              [skirmish.wui.components.topbar :refer [top-bar]]
              [reagent.session :as session]
              [cljs-http.client :as http]
              [cljs.core.async :refer [<!] :refer-macros [go]]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]))

;; -------------------------
;; Views
(enable-console-print!)

(defonce state (atom {}))

(defn login [email]
  (go (let [resp (<! (http/get (str "http://localhost:1605/api/1/users/" email)))]
        (if (= (:status resp) 200)
          (do
            (println "Logging in as:" (-> resp :body type))
            (session/put! :current-page #'pages/home-page)
            (swap! state #(into % {:page :home :user (:body resp)})))
          (do
            (println "Cannot log in as" email)
            (swap! state #(dissoc % :user)))))))

;; -------------------------
;; Routes


(secretary/defroute "/todo" []
                    (session/put! :current-page #'pages/todo-page)
                    (swap! state  #(into % {:page :todo})))

(secretary/defroute "/" []
                    (session/put! :current-page #'pages/home-page)
                    (swap! state #(into % {:page :home})))

(secretary/defroute "/home/:email" [email]
                    (login email))

(secretary/defroute "/home/" []
                    (session/put! :current-page #'pages/home-page)
                    (swap! state #(into % {:page :home :user nil})))

(secretary/defroute "/users" []
                    (session/put! :current-page #'pages/users-page)
                    (swap! state #(into % {:page :users})))

(secretary/defroute "/securities" []
                    (session/put! :current-page #'pages/securities-page)
                    (swap! state #(into % {:page :securities})))

(secretary/defroute "/portfolio" []
                    (session/put! :current-page #'pages/portfolio-page)
                    (swap! state #(into % {:page :portfolio})))

(secretary/defroute "/otc" []
                    (session/put! :current-page #'pages/otc-page)
                    (swap! state #(into % {:page :otc})))



(secretary/defroute "/create-order" []
                    (session/put! :current-page #'pages/create-order-page)
                    (swap! state #(into % {:page :create-order})))



(secretary/defroute "/securities/:sec-id" [sec-id]
                    (session/put! :current-page #'pages/security-page)
                    (swap! state #(into % {:page :securities :sec-id (keyword sec-id)})))

;; -------------------------
;; Initialize app


(defn current-page []
  "This is the basic page setup."
  [:div
   [top-bar state]
   [:div
    [:div.columns
     [:div.column.is-8.is-offset-2
      [(session/get :current-page) @state]]]]])



(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (mount-root))
