(ns skirmish.wui.components.topbar)


(defn font-awesome [class]
  [:i {:class (str "fa fa-" class) :aria-hidden true}])


(defn- top-bar-class [state page]
  (when (= (:page @state) page)
    "is-active"))


(defn- user-dropdown [state]
  (when (:user @state)
    (let [icon [:i.fa.fa-sign-out {:aria-hidden true}]
          email (get-in @state [:user :skirmish.broker.user/email])]
      [:div.navbar-item.has-dropdown.is-hoverable
       [:a.navbar-link {} (str "[ " email " ]")]
       [:div.navbar-dropdown
        [:a.navbar-item {:href "/home/"} icon  " logout"]]])))


(defn- login [state]
  (let [icon [:i.fa.fa-sign-out {:aria-hidden true}]]
    [:a.navbar-item.is-tab {:href "/"} icon " login"]))


(defn user-placeholder [state]
  (if (:user @state)
    [user-dropdown state]
    [login state]))


(defn top-bar [state]
  [:nav.navbar.is-dark.is-boxed {:style {:margin-bottom "1em"}}
   [:div.container
    [:div.navbar-start
     [:a.navbar-item.is-tab {:href "/" :class (top-bar-class state :home)} "Home"]
     [:a.navbar-item.is-tab {:href "/securities" :class (top-bar-class state :securities)} "Securities"]
     [:a.navbar-item.is-tab {:href "/create-order" :class (top-bar-class state :create-order)} "Create Order"]
     [:a.navbar-item.is-tab {:href "/otc" :class (top-bar-class state :otc)} "Over The Counter"]
     [:a.navbar-item.is-tab {:href "/portfolio" :class (top-bar-class state :view-portfolio)} "Portfolio"]
     [:a.navbar-item.is-tab {:href "/users" :class (top-bar-class state :users)} "Users"]
     [:a.navbar-item.is-tab {:href "/todo" :class (top-bar-class state :todo)} "Todo"]]
    [:div.navbar-end
     [:a.navbar-item {} "Skirmish Shitty Exchange"]
     [user-placeholder state]]]])
