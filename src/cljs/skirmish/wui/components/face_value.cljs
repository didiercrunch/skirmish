(ns skirmish.wui.components.face-value
  (:require [skirmish.wui.components.secunit :refer [secunit]]))

(defn face-value [amount sec-id]
  (when (= sec-id :dapm)
    (secunit amount sec-id)))
