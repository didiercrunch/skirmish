(ns skirmish.wui.components.portfolio-page
  (:require [reagent.core :refer [atom]]
            [cljs-http.client :as http]
            [skirmish.wui.components.face-value :refer [face-value]]
            [cljs.core.async :refer [<!] :refer-macros [go] :as async]
            [skirmish.wui.components.secunit :refer [secunit]]))

(defn make-portfolio-summary-table [portfolio]
  [:table
   [:thead
    [:tr [:th "Security"] [:th "Amount"] [:th "Face Value"]]]
   [:tbody
    (for [{sec-summary :account} portfolio]
      [:tr {:key (:account-public-key sec-summary)}
       [:td (:sec-id sec-summary)]
       [:td [secunit (:amount sec-summary) (-> sec-summary :sec-id keyword)]]
       [:td [face-value (:amount sec-summary) (-> sec-summary :sec-id keyword)]]])]])


(defn get-accounts-information [urls a-accounts]
  (go
    (let [ar (async/merge (map #(http/get % {:with-credentials? false}) urls))
          accounts (map :body (<! (async/into [] ar)))]
      (reset! a-accounts accounts))))

(defn page-wrapper [accounts]
  [:div.content
   [:h5 "View all the assets you own."]
   [make-portfolio-summary-table accounts]])


(defn page [{{accounts :skirmish.broker.user/accounts} :user}]
  (let [urls (map :skirmish.broker.account/url accounts)
        a-accounts (atom [])]
    (get-accounts-information urls a-accounts)
    #(page-wrapper @a-accounts)))
