(ns skirmish.wui.components.home-page
  (:require [cljs-http.client :as http]
            [reagent.core :refer [atom]]
            [skirmish.wui.util :refer [email?]]
            [cljs.core.async :refer [<!] :refer-macros [go]]
            [secretary.core :as secretary]))

(defn create-user [email]
  (go
    (let [url "http://localhost:1605/api/1/users"
          resp (<! (http/post url {:edn-params {:email email}}))]
      (secretary/dispatch! (str "/home/" email)))))


(defn login [state]
  (let [login-field (atom (get-in state [:user :skirmish.broker.user/email] ""))
        on-change #(reset! login-field (-> % .-target .-value))]
    (fn []
      (let [disabled? (-> @login-field email? not)
            href (if (not disabled?) (str "/home/" @login-field) nil)]
        [:div.field
         [:div.control
          [:label.label "Email"]
          [:div.field.is-grouped
           [:input.input.is-success {:type "email"
                                     :value @login-field
                                     :on-change on-change
                                     :placeholder "boris.johnson@redneck.co.uk"
                                     :style {:margin-right "1em"}}]
           [:div.control
            [:a.button.is-link.has-background-primary {:href href
                                                       :disabled disabled?} "login"]]
           [:div.control
            [:button.button.is-link.has-background-primary {:disabled disabled?
                                                            :on-click #(create-user @login-field)} "create"]]]]]))))


(defn page [state]
  [:div
   [:section.section
    [:div.columns
     [:div.container
      [:img {:src "/images/amsterdam.svg"}]]]
    [:div.columns.is-centered
     [login state]]]])