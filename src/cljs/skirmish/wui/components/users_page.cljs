(ns skirmish.wui.components.users-page
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [<!] :as async]
            [reagent.core :refer [atom]])
  (:require-macros [cljs.core.async.macros  :refer [go]]))


(def users-table-headers
  [:thead.has-background-grey-dark
   [:tr
    [:th.has-text-white-bis "User email"]
    [:th.has-text-white-bis "Public wallet id"]]])


(defn create-row [user]
    [:tr.has-text-white-bis {:key (:email user)}
     [:td (:email user)]
     [:td "key"]])

(defn create-users-table [users]
  [:table.table.is-hoverable.is-fullwidth
   users-table-headers
   [:tbody
    (map create-row users)]])

(defn get-users [users-a]
  (go
    (let [url "http://localhost:1605/api/1/users"
          resp (<! (http/get url))]
      (reset! users-a (-> resp :body :users)))))

(defn users-table []
  (let [users (atom [])]
    (get-users users)
    #(create-users-table @users)))

(defn page [state]
  [:div
   [:div.content
    [:h5 "Users"]
    [users-table]]])
