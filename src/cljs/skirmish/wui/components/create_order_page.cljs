(ns skirmish.wui.components.create-order-page
  (:require [reagent.core :refer [atom]]))

(defn set-value-in-atom [atm key evt]
  (swap! atm #(assoc % key (-> evt .-target .-value))))



(defn select-type [order]
  [:div.control
   [:div.select
    [:select {:on-change (partial set-value-in-atom order :type)}
     [:option {:value :buy} "Buy"]
     [:option {:value :sell} "Sell"]]]])

(defn select-securities [order]
  [:div.control
   [:div.select
    [:select {:on-change (partial set-value-in-atom order :sec-id)}
     [:option {:value :nype} ":nype"]
     [:option {:value :osko} ":osko"]
     [:option {:value :oops} ":oops"]]]])

(defn select-quantities [order]
  [:div.control
   [:input.input {:type "number"
                  :on-change (partial set-value-in-atom order :quantity)}]])

(def action-button
  [:div.control
   [:button.button.is-primary "Submit"]])


(defn page []
  (let [order (atom {})]
    (fn []
      [:div
       [:div.columns
        [:div.column.content
         [:div.columns.is-centered
          [:div
           [:div.field.has-addons
             (select-type order)
             (select-securities order)
             (select-quantities order)
             action-button]]]]]
       [:div.columns
        [:div.column.content
         [:pre (str @order)]]]])))

