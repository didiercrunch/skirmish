(ns skirmish.wui.components.security-page
  (:require [skirmish.wui.util :refer [pp-int]]
            [reagent.core :refer [atom]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!] :refer-macros [go]]))


(defn get-ledget-information [sec-info sec-id]
  (go
    (let [url (str "http://localhost:1604/api/1/ledger/securities/" (name sec-id))
          resp (<! (http/get url {:with-credentials? false}))]
      (reset! sec-info (:body resp)))))
  

(defn create-ledger-table [sec-info]
  [:div
   [:h1 (:name sec-info)]
   [:cite (:description sec-info)]
   [:table
    [:tbody
     [:tr
      [:td "Security ID"] [:td (:sec-id sec-info)]]
     [:tr
      [:td "Type"] [:td (:type sec-info)]]
     (when-let [symbol (:symbol sec-info)]
       [:tr
        [:td "Symbol"] [:td symbol]])
     [:tr
      [:td "Interest Rate"] [:td (-> sec-info :interest-rate (pp-int 2) (str " %"))]]
     [:tr
      [:td "Total Volume"] [:td (-> sec-info :volume (pp-int (:number-of-decimal sec-info)) (str " " (:symbol sec-info)))]]]]])


(defn ledger-table [sec-id]
  (let [sec-info (atom {})]
    (get-ledget-information sec-info sec-id)
    #(create-ledger-table @sec-info)))


(defn page [state]
  [:div.content
   [ledger-table (:sec-id state)]])
