(ns skirmish.wui.components.otc-page
  (:require [reagent.core :refer [atom]]
            [skirmish.wui.components.securities-page :refer [get-securities]]
            [skirmish.wui.util :refer [email?]]))


(defn set-amount [state event]
  (let [inserted-value (-> event .-target .-value)
        new-value (first (re-find #"^\d+(\.\d*)?" inserted-value))]
    (swap! state #(assoc % :value new-value))))

(defn set-email [state event]
  (let [new-email (-> event .-target .-value)]
    (swap! state #(assoc % :email new-email))))

(defn set-sec-id [state event]
  (let [sec-id (-> event .-target .-value)]
    (swap! state #(assoc % :sec-id sec-id))))



(defn ready-to-transfer? [state]
  (and (-> state :value js/parseInt pos?)
       (-> state :email email?)))


(defn select-securities-for [securities on-change]
  [:select {:on-change on-change}
   (for [security securities]
     [:option {:value (:sec-id security) :key  (:sec-id security)} (:sec-id security)])])


(defn select-security [on-change]
  (let [securities (atom [])]
    (get-securities securities)
    #(select-securities-for @securities on-change)))

(defn direct-send []
  (let [state (atom {})]
    (fn []
      [:div
       [:div.content
        [:p "Send securities to another person."]]
       [:form.form
        [:div.field.has-addons.has-addons-centered
         [:div.control
          [:span.select
           [select-security (partial set-sec-id state)]]]
         [:div.control
          [:input.input {:type "text"
                         :value (:value @state)
                         :on-change (partial set-amount state)
                         :placeholder "110.90"}]]
         [:div.control
          [:input.input {:type "text"
                         :value (:email @state)
                         :on-change (partial set-email state)
                         :placeholder "boris.johnson@redneck.co.uk"}]]
         [:a.button.is-primary {:style {:marginLeft "1em"}
                                :disabled (-> @state ready-to-transfer? not)} "transfer"]]]])))

(defn page [state]
  [:div
   [:div.content
    [:h5 "Do transaction without passing by the exchange."]]

   [direct-send]])
