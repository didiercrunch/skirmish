(ns skirmish.wui.components.secunit
  (:require [reagent.core :refer [atom]]
            [skirmish.wui.util :refer [pp-int]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!] :refer-macros [go] :as async]))


(defn get-security-definition [sec sec-id]
  (go
    (let [url (str "http://localhost:1604/api/1/ledger/securities/" (name sec-id))
          resp (<! (http/get url {:with-credentials? false}))]
      (reset! sec (:body resp)))))


(defn- pp-amount [amount sec]
  (when (seq sec)
    (let [{sec-id :sec-id number-of-decimal :number-of-decimal symbol :symbol} sec
          href (str "/securities/" sec-id)]
      [:a {:href href} (pp-int amount (or number-of-decimal 0)) " "
                       (or symbol "")])))

(defn secunit [amount sec-id]
  (let [sec (atom {})]
    (get-security-definition sec sec-id)
    #(pp-amount amount @sec)))