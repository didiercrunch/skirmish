(ns skirmish.wui.components.todo-page)



(def on-it {:style {:font-weight "bold"}})

(def done {:style {:text-decoration "line-through"}})


(defn filter-out-done [n]
  (let [state (volatile! n)]
    (fn [item]
      (when (= (second item) done)
        (vswap! state dec))
      (or (not= (second item) done) (neg? @state)))))


(defn todo-list [& items]
  (let [filter-fn (filter-out-done (- (count items) 20))
        item-to-show (filter filter-fn items)]
    [:ol
     (for [item item-to-show]
       item)]))

(defn page []
  [:div.content
   [:h5 "This still to do in this project.  This is a jira for the poor (or for the geek)."]
   [:ol
    [:li "Broker"
     [:p "the brooker should use the exchange-gateway and ledge as apis.  Nothing else."]
     [todo-list
      [:li done "implement basic login in the wui"]
      [:li done "implement basic web server"]
      [:li done "implement stupid login on server side."]
      [:li done "link the logic of the client-server login logic"]
      [:li done "add the notion of 'accounts' in the server.  An
                  user should have many accounts; one for each security
                  he owns"]
      [:li done "make the " [:a {:href "/portfolio"} "portfolio"] " page representing the use accounts"]
      [:li done "make a somewhat pretty " [:a {:href "/securities/dapm"} "security"] " page"]
      [:li done "create two stocks"]
      [:li done "add stocks to didier@gmail.com"]
      [:li done "make something for urls on the backend"]
      [:li done "add a wui for OTC"]
      [:li "fix the stupid chat.  This is an important feature for OTC users."]
      [:li "make something that allows to send securities from one user to another"]
      [:li "move 'routes' in 'rest'"]
      [:li "add a little bit of professionalism on top of the broker api"]
      [:li "make the api support transit and edn"]
      [:li done "implements interest rate in ledger."]
      [:li done "Make sure the ledger can have accounts"]
      [:li done "when a new user signs in, he should receive some sort of money"]
      [:li "implement send securities to friends"]
      [:li "implement buy/sell securities."]
      [:li "Use ledger as source of through in each sub server."]
      [:li "Update web ui for integrating securities with ledger."]
      [:li "implement transactions in ledger."]
      [:li "implement escrow."]]]]])
