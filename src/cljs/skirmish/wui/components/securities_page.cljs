(ns skirmish.wui.components.securities-page
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [<!] :as async]
            [reagent.core :refer [atom]])
  (:require-macros [cljs.core.async.macros  :refer [go]]))

(def table-head
  [:thead.has-background-grey-dark
   [:tr
    [:th.has-text-white-bis "sec-id"]
    [:th.has-text-white-bis "name"]
    [:th.has-text-white-bis "description"]]])

(defn create-row [sec]
  (let [link [:a {:href (str "/securities/" (-> sec :sec-id name))} (-> sec :sec-id name)]]
    [:tr.has-text-white-bis {:key (:sec-id sec)}
     [:td link]
     [:td (:name sec)]
     [:td (:description sec)]]))

(defn get-all-securities-urls []
  (let [ret (async/chan)]
    (go (let [url "http://localhost:1604/api/1/ledger/securities"
              response (<! (http/get url {:with-credentials? false}))
              urls (map :url (get-in response [:body :securities]))]
          (async/onto-chan ret urls)))
    ret))

(defn get-all-securities []
  (let [ret (async/chan)
        urls (get-all-securities-urls)]
    (async/go-loop [url (async/<! urls)]
                   (when-let [url (async/<! urls)]
                     (async/go
                       (let [sec (:body (<! (http/get url {:with-credentials? false})))]
                         (println sec)
                         (async/>! ret sec)))
                     (recur url)))
    ret))

(defn get-securities [a-securities]
  (let [securities (get-all-securities)]
    (async/go-loop [security (async/<! securities)]
                   (when (some? security)
                     (swap! a-securities #(conj % security))
                     (recur (async/<! securities))))))


(defn create-currencies-table [securities]
  [:div
   [:div.content
    [:h3 "Currencies"]
    [:p "A " [:em "currency"] " is currency that pays interest rate in
         its own nomination."]]
   [:table.table.is-hoverable.is-fullwidth
    table-head
    [:tbody
     (for [sec securities :when (= (:type sec) "currency")] (create-row sec))]]])

(defn create-equities-table [securities]
  [:div
   [:div.content
    [:h3 "Equities"]
    [:p "An " [:em "equity"] " is a type of security that pays dividends.  In
         real life, an equity (or stock) is a share in a company ownership.
         When the company make profits, it shares the its profits between its stock
         holder in a pro rata fashion."]]
   [:table.table.is-hoverable.is-fullwidth
    table-head
    [:tbody
     (for [sec securities :when (= (:type sec) "equity")] (create-row sec))]]])

(defn create-securities-table [securities]
  [:div
   [create-currencies-table securities]
   [:br]
   [create-equities-table securities]])

(defn security-table []
  (let [securities (atom [])]
    (get-securities securities)
    #(create-securities-table @securities)))

(defn page [state]
  [:div
   [:div.content
    [:h5 "View the available securities for trading."]]
   [security-table]])