(ns skirmish.wui.util)

(defn right-pad [s o n]
  "right pad s with o until length is greater or
  equal to n."
  (if (<= n (count s))
    s
    (recur (str s o) o n)))

(defn left-pad [s o n]
  "left pad s with o until length is greater or
  equal to n."
  (if (<= n (count s))
    s
    (recur (str o s) o n)))

(defn pp-int
  "returns the string representing n with the spaces at
  the proper place."
  ([n]
   (cond
     (neg? n) (str "-" (pp-int (- n)))
     (< n 1000) (str n)
     :else (str (pp-int (quot n 1000)) " " (left-pad (str (mod n 1000)) "0" 3))))
  ([n dec]
   (if (<= dec 0)
     (pp-int n)
     (let [i (int (Math/pow 10 dec))]
       (str (pp-int (quot n i)) "," (right-pad (str (mod n i)) "0" dec))))))

(defn email? [s]
  (let [email-pattern #"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"]
    (->> s str (re-matches email-pattern) some?)))

