(ns skirmish.wui.pages
  (:require [skirmish.wui.components.todo-page]
            [skirmish.wui.components.home-page]
            [skirmish.wui.components.portfolio-page]
            [skirmish.wui.components.create-order-page]
            [skirmish.wui.components.securities-page]
            [skirmish.wui.components.security-page]
            [skirmish.wui.components.otc-page]
            [skirmish.wui.components.users-page]))

(def home-page skirmish.wui.components.home-page/page)

(def todo-page skirmish.wui.components.todo-page/page)

(def securities-page skirmish.wui.components.securities-page/page)

(def create-order-page skirmish.wui.components.create-order-page/page)

(def portfolio-page skirmish.wui.components.portfolio-page/page)

(def security-page skirmish.wui.components.security-page/page)

(def otc-page skirmish.wui.components.otc-page/page)

(def users-page skirmish.wui.components.users-page/page)