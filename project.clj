(defproject skirmish "0.1.0-SNAPSHOT"
  :description "A stock exchange simulator."
  :url "https://gitlab.com/didiercrunch/skirmish/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :resource-paths ["resources"
                   "target/cljsbuild"]

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "1.3.610"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [com.datomic/datomic-free "0.9.5697"]
                 [com.google.guava/guava "22.0"]
                 ;[javax.xml.bind/jaxb-api "2.2.11"]

                 ;; server
                 [ring-cors "0.1.13"]
                 [ring/ring-defaults "0.3.2"]
                 ;[ring/ring-jetty-adapter "1.8.0"]
                 [javax.servlet/javax.servlet-api "3.1.0"]
                 [aero "1.1.3"]
                 [com.taoensso/timbre "4.10.0"]
                 [org.clojure/tools.reader "1.3.3"]
                 [http-kit "2.4.0"]
                 [compojure "1.6.2"]

                 ;; backends
                 [spootnik/kinsky "0.1.25"]

                 ;;utils
                 [slingshot "0.12.2"]
                 [orchestra "2020.07.12-1"]
                 [cheshire "5.10.0"]
                 [com.stuartsierra/frequencies "0.1.0"]
                 [rm-hull/infix "0.3.3"]

                 ;; wui
                 [reagent "0.8.1"]
                 [reagent-utils "0.3.1"]
                 [hiccup "1.0.5"]

                 ;; cljs
                 [org.clojure/clojurescript "1.10.773" :scope "provided"]
                 [cljs-http "0.1.46"]
                 [secretary "1.2.3"]
                 [venantius/accountant "0.2.2" :exclusions [org.clojure/tools.reader]]]

  :plugins [[lein-midje "3.2.1"]
            [lein-npm "0.6.2"]
            [lein-environ "1.1.0"]  ;; maybe to remove
            [lein-cljsbuild "1.1.8"]
            [lein-asset-minifier "0.4.6" :exclusions [org.clojure/clojure]]]

  :java-source-paths ["src/java"]

  :npm {:dependencies [[bulma "0.7.2"]
                       [font-awesome "4.7.0"]]}

  :ring {:handler skirmish.wui.handler/app
         :uberwar-name "skirmish.war"}

  :min-lein-version "2.5.0"
  :uberjar-name "skirmish.jar"
  :main skirmish.core
  :clean-targets ^{:protect false}
  [:target-path
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-to]]

  :source-paths ["src/clj" "src/cljc"]

  :minify-assets
  {:assets
   {"resources/public/css/site.min.css" "resources/public/css/site.css"}}

  :cljsbuild
  {:builds {:min
            {:source-paths ["src/cljs" "src/cljc" "env/prod/cljs"]
             :compiler
             {:output-to        "target/cljsbuild/public/js/app.js"
              :output-dir       "target/cljsbuild/public/js"
              :source-map       "target/cljsbuild/public/js/app.js.map"
              :optimizations :advanced
              :pretty-print  false}}
            :app
            {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
             :figwheel {:on-jsload "skirmish.wui.core/mount-root"}
             :compiler
             {:main "skirmish.wui.dev"
              :asset-path "/js/out"
              :output-to "target/cljsbuild/public/js/app.js"
              :output-dir "target/cljsbuild/public/js/out"
              :source-map true
              :optimizations :none
              :pretty-print  true}}


            :devcards
            {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
             :figwheel {:devcards false}
             :compiler {:main "skirmish.wui.cards"
                        :asset-path "js/devcards_out"
                        :output-to "target/cljsbuild/public/js/app_devcards.js"
                        :output-dir "target/cljsbuild/public/js/devcards_out"
                        :source-map-timestamp true
                        :optimizations :none
                        :pretty-print true}}}}



  :figwheel
  {:http-server-root "public"
   :server-port 3449
   :nrepl-port 7002
   :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"]

   :css-dirs ["resources/public/css"]
   :ring-handler skirmish.wui.handler/app}

  :test-paths ["test/clj" "test/cljs"]


  :sass {:source-paths ["src/sass"]
         :target-path "resources/public/css"}

  :profiles {:dev {
                   :dependencies [[midje "1.9.9"]
                                  [stylefruits/gniazdo "1.0.1"] ;; only used in benchmark
                                  [clj-http "3.9.0"]  ;; only used in benchmark

                                  [binaryage/devtools "0.9.10"]
                                  [ring/ring-mock "0.3.2"]
                                  [ring/ring-devel "1.6.3"]
                                  [prone "1.5.2"]
                                  [figwheel-sidecar "0.5.16"]
                                  [org.clojure/tools.nrepl "0.2.13"]
                                  [com.cemerick/piggieback "0.2.2"]
                                  [devcards "0.2.3" :exclusions [cljsjs/react]]
                                  [pjstadig/humane-test-output "0.8.3"]

                                  ;; To silence warnings from sass4clj dependecies about missing logger implementation
                                  [org.slf4j/slf4j-nop "1.7.25"]]


                   :source-paths ["env/dev/clj"]
                   :plugins [[lein-figwheel "0.5.16"]
                             [deraen/lein-sass4clj "0.3.1"]]


                   :injections [(require 'pjstadig.humane-test-output)
                                (pjstadig.humane-test-output/activate!)]

                   :env {:dev true}}

             :uberjar {:hooks [minify-assets.plugin/hooks]
                       :source-paths ["env/prod/clj"]
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :env {:production true}
                       :aot :all
                       :omit-source true}})
