FROM clojure
MAINTAINER Didier Amyot <didier.amyot@gmail.com>

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages
RUN lein deps

EXPOSE 8080

CMD ["lein", "run"]
