(ns skirmish.test-PersistentSortedList
  (:require [midje.sweet :refer :all]
            [skirmish.util :refer [sorted-list sorted-list-by]]))



(facts "test-match-buy-order-in-sell-book"
  (fact "constructing"
    (map nil? (sorted-list)) => empty?)
    ;;  The following are tests that fails...
    ;(vec (sorted-list)) => empty?)
  (fact "Test ordering."
        (sorted-list 1 2 3) => (sorted-list 3 2 1)
        (sorted-list 1 2 2 3) => (sorted-list 3 2 1 2))
  (fact "count"
        (count (sorted-list)) => 0
        (count (sorted-list 1 2 3)) => 3
        (count (sorted-list 1 2 3 2)) => 4)
  (fact "rest"
        (rest (sorted-list 1 2 3)) => (sorted-list 2 3)
        (rest (sorted-list 1 2 3 2)) => (sorted-list 2 2 3)
        (rest (sorted-list 1)) => (sorted-list)
        (rest (sorted-list)) => empty?
        (str (type (rest (sorted-list)))) => "class skirmish.PersistentSortedList")
  (fact "next"
        (next (sorted-list 1 2 3)) => (sorted-list 2 3)
        (next (sorted-list 1 2 3 2)) => (sorted-list 2 2 3)
        (next (sorted-list 1)) => nil
        (next (sorted-list)) => nil)
  (fact "drop"
        (drop 2 (sorted-list 1 2 3)) => (sorted-list 3)
        (drop 3 (sorted-list 1 2 3)) => (list))
  (fact "empty"
        (empty (sorted-list 2 3 4)) => (sorted-list)
        (into (empty (sorted-list-by #(- %2 %1) 2 3 4)) [20 30]) => (sorted-list-by #(- %2 %1) 20 30))
  (fact "reduce"
        (reduce + (sorted-list 1 2 3)) => 6
        (reduce + 10 (sorted-list 1 2 3)) => 16)
  (fact "peek/pop"
        (peek (sorted-list 1 2 3)) => 1
        (peek (sorted-list 1 1 1 2 3)) => 1
        (peek (sorted-list)) => nil
        (pop (sorted-list 1 2 3)) => (sorted-list 2 3)
        (pop (sorted-list 1 1 2 3)) => (sorted-list 1 2 3)
        (pop (sorted-list 1 1 1 2 3)) => (sorted-list 1 1 2 3)
        (pop (sorted-list)) => (throws IllegalStateException))

  (fact "count after change"
        (let [l (sorted-list 1 2 3)]
          (count (conj l 4)) => 4
          (count (conj l 3)) => 4
          (count (cons 3 l)) => 4
          (count (rest l)) => 2
          (count (pop l)) => 2))
  (fact "sorted-list-by"
        (sorted-list-by > 1 2 3) => [3 2 1]))
