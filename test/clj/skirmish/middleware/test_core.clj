(ns skirmish.middleware.test-core
  (:require [midje.sweet :refer :all]
            [clojure.core.async :refer [>!! <!!] :as async]
            [skirmish.middleware.core :refer :all]))

(defn all? [xf]
  #(every? true? (sequence xf %)))

(facts "we should have a async/chan base middleware for developing."
  (fact "It should exchanges for each security in the config"
    (let [config {}
          securities [{:sec-id :molly :type :equity} {:sec-id :pepper :type :currency} {:sec-id :molly :type :equity :disabled true}]
          securities-exchange (create-securities-api-for config)
          securities-submitter (:securities-submitter securities-exchange)
          exchanges-mult (create-exchange-apis-for config securities-exchange)
          exchanges (async/chan)
          sec-id-and-disabled (juxt :sec-id :disabled)]
      (async/tap exchanges-mult exchanges)
      (>!! securities-submitter (first securities))

      (-> exchanges <!! sec-id-and-disabled) => [:molly false]
      (>!! securities-submitter (second securities))
      (-> exchanges <!! sec-id-and-disabled) => [:pepper false]
      (>!! securities-submitter (last securities))
      (-> exchanges <!! sec-id-and-disabled) => [:molly true])))