(ns skirmish.ledger.test-core
  (:require [midje.sweet :refer :all]
            [skirmish.ledger.data-layer :as data-layer]
            [skirmish.ledger.controllers :as controllers]
            [skirmish.ledger.accounts :refer [create-connection schema]]
            [skirmish.rest.util :refer [build-response]]
            [skirmish.ledger.core :refer :all]
            [skirmish.rest.util :refer [edn->stream]]
            [datomic.api :as d]
            [clojure.edn]
            [clojure.spec.alpha :as s]
            [infix.macros :refer [infix]]))


(defn spec-check [spec]
  #(s/valid? spec %))

(defn body [resp]
  (-> resp :body clojure.edn/read-string))

(s/def ::ok #(<= 200 (:status %) 299))

(s/def ::status-404 #(= (:status %) 404))

(s/def ::status-400 #(= (:status %) 400))


(defn create-conn []
  (let [mem-url "datomic:mem://skirmish"]
    (d/delete-database mem-url)
    (create-connection mem-url false)))



(defn location? [path]
  #(-> % :headers (get "location") (clojure.string/ends-with? path)))


(def sec->hash-set #(apply hash-set %))

(def mock-securities [[:sec-1 100 :equity "first equity" "rot13"]
                      [:sec-2 200 :equity "second equity" "rot13"]
                      [:sec-3 300 :equity "third equity" "rot13"]])

(def mock-currencies [[:zimbabwean-dollar 1000000 :currency "zimbabwean dollar" "rot13" "zd"]])

;;
(defn create-some-securities [conn]
  (let [cs (controllers/create-security conn)
        create-security-1 {:body (edn->stream (get mock-securities 0)) :headers {"accept" "application/edn"}}
        create-security-2 {:body (edn->stream (get mock-securities 1)) :headers {"accept" "application/edn"}}
        create-security-3 {:body (edn->stream (get mock-securities 2)) :headers {"accept" "application/edn"}}
        create-security-4 {:body (edn->stream (first mock-currencies)) :headers {"accept" "application/edn"}}]
    (->> create-security-1 cs (s/assert ::ok))
    (->> create-security-2 cs (s/assert ::ok))
    (->> create-security-3 cs (s/assert ::ok))
    (->> create-security-4 cs (s/assert ::ok))
    conn))

(defn create-some-accounts [conn n]
  (let [create-emit-req (fn [amount account-id sec-id] {:body (edn->stream [amount account-id]) :route-params {:sec-id sec-id}})
        sec-1-reqs (for [i (range 1 (inc n))] (create-emit-req (* i 100) (str i "-sec-1") "sec-1"))
        sec-2-reqs (for [i (range 1 (inc n))] (create-emit-req (* i 100) (str i "-sec-2") "sec-2"))
        reqs (concat sec-1-reqs sec-2-reqs)]
    (loop [reqs reqs]
      (when (seq reqs)
        (let [es (controllers/emit-securities conn)
              req (first reqs)]
          (->> req es (s/assert ::ok))
          (recur (rest reqs)))))
    conn))


(defn create-some-securities-and-accounts [conn n-accounts-by-currency]
  (-> conn create-some-securities (create-some-accounts n-accounts-by-currency)))


(facts "about the securities"
       (fact "We should be able to create a security"
             (let [conn (create-conn)
                   security [:new-currency 100 :currency "currency for test" "rot13" "NC"]
                   request {:body (edn->stream security) :headers {"accept" "application/edn"}}
                   resp ((controllers/create-security conn) request)]
               resp => (spec-check ::ok)
               resp => (location? "/api/1/ledger/securities/new-currency")))

       (fact "We should not be able to create a security that already exists"
             (let [conn (create-conn)
                   security [:new-currency 100 :currency "currency for test" "rot13" "NC"]
                   request {:body (edn->stream security) :headers {"accept" "application/edn"}}
                   resp-1 ((controllers/create-security conn) request)
                   resp-2 ((controllers/create-security conn) request)]
               resp-1 => (spec-check ::ok)
               resp-2 => (spec-check ::status-400)))

       (fact "We should be able to update a security when it exists"
             :todo => :todo)

       (fact "we should be able to get-security if it does exists"
             (let [conn (create-conn)
                   security [:new-currency 100 :currency "currency for test" "rot13" "NC"]
                   create-request {:body (edn->stream security) :headers {"accept" "application/edn"}}
                   _ ((controllers/create-security conn) create-request)
                   request {:route-params {:sec-id :new-currency} :headers {"accept" "application/edn"}}
                   resp ((controllers/get-security conn) request)
                   expt {:sec-id            :new-currency
                         :interest-rate     100
                         :name              nil
                         :description       nil
                         :number-of-decimal nil
                         :symbol            nil
                         :type              nil
                         :volume            0}]
               resp => (spec-check ::ok)
               (body resp) => (contains expt)))

       (fact "we should not be able to get-security if it does not exists"
             (let [conn (create-conn)
                   request {:route-params {:sec-id :new-currency} :headers {"accept" "application/edn"}}
                   resp ((controllers/get-security conn) request)]
               resp => (spec-check ::status-404)
               resp => #(-> % :sec-id nil?)))

       (fact "we should be able to get a list of all securities"
             (let [conn (create-conn)
                   request {:route-params {} :headers {"accept" "application/edn"}}
                   empty-res ((controllers/get-available-securities conn) request)
                   new-currency [:new-currency 100 :currency "currency for test" "rot13" "NC"]
                   create-security-1 {:body (edn->stream new-currency) :headers {"accept" "application/edn"}}
                   _ ((controllers/create-security conn) create-security-1)
                   one-res ((controllers/get-available-securities conn) request)]
               empty-res => (spec-check ::ok)
               one-res => (spec-check ::ok)
               (body empty-res) => {:securities []}
               (body one-res) => {:securities [{:sec-id :new-currency :url "http://localhost:1604/api/1/ledger/securities/new-currency"}]}))

       (fact "it should add default currencies when creating new db"
             (let [mem-url "datomic:mem://skirmish-in-test"
                   _ (d/delete-database mem-url)
                   conn (create-connection mem-url true)
                   request {:route-params {} :headers {"accept" "application/edn"}}
                   resp ((controllers/get-available-securities conn) request)]
               (->> resp body :securities (map :sec-id) sort) => [:apac :apax :apbm :apdr :apmh :dapm])))


(facts "about the emitting securities"
       (fact "should not work if invalid request"
             (let [conn (create-some-securities (create-conn))
                   req-1 {:body (edn->stream [10 :not-a-string]) :route-params {:sec-id "sec-1"} :headers {"accept" "application/edn"}}
                   req-2 {:body (edn->stream [-10 "foo"]) :route-params {:sec-id "sec-1"} :headers {"accept" "application/edn"}}
                   req-3 {:body (edn->stream [90 "account-id"]) :route-params {:sec-id "invalid-sec-id"} :headers {"accept" "application/edn"}}]
               ((controllers/emit-securities conn) req-1) => (spec-check ::status-400)
               ((controllers/emit-securities conn) req-2) => (spec-check ::status-400)
               ((controllers/emit-securities conn) req-3) => (spec-check ::status-404)))

       (fact "should create new account if targeted account does not exists"
             (let [conn (create-some-securities (create-conn))
                   get-account-req {:route-params {:account-id "new-account-id"} :headers {"accept" "application/edn"}}
                   emit-sec-req {:body (edn->stream [90 "new-account-id"]) :route-params {:sec-id "sec-1"} :headers {"accept" "application/edn"}}]
               ((controllers/get-account conn) get-account-req) => (spec-check ::status-404)
               ((controllers/emit-securities conn) emit-sec-req) => (spec-check ::ok)
               (body ((controllers/get-account conn) get-account-req)) => #(-> % :account :amount (= 90))))

       (fact "should add to account if targeted account exists"
             (let [conn (create-some-securities (create-conn))
                   get-account-req {:route-params {:account-id "new-account-id"} :headers {"accept" "application/edn"}}
                   emit-sec-req-1 {:body (edn->stream [90 "new-account-id"]) :route-params {:sec-id "sec-1"} :headers {"accept" "application/edn"}}
                   emit-sec-req-2 {:body (edn->stream [100 "new-account-id"]) :route-params {:sec-id "sec-1"} :headers {"accept" "application/edn"}}]
               ((controllers/get-account conn) get-account-req) => (spec-check ::status-404)
               ((controllers/emit-securities conn) emit-sec-req-1) => (spec-check ::ok)
               ((controllers/emit-securities conn) emit-sec-req-2) => (spec-check ::ok)
               (body ((controllers/get-account conn) get-account-req)) => #(-> % :account :amount (= 190)))))


(facts "about accounts"
       (fact "we should be able to get list of all accounts"
             (let [conn (create-some-securities-and-accounts (create-conn) 2)
                   req {:route-params {} :headers {"accept" "application/edn"}}
                   expt #{{:account-public-key "1-sec-1" :url "http://localhost:1604/api/1/ledger/accounts/1-sec-1"}
                          {:account-public-key "1-sec-2" :url "http://localhost:1604/api/1/ledger/accounts/1-sec-2"}
                          {:account-public-key "2-sec-1" :url "http://localhost:1604/api/1/ledger/accounts/2-sec-1"}
                          {:account-public-key "2-sec-2" :url "http://localhost:1604/api/1/ledger/accounts/2-sec-2"}}]
               ((controllers/get-available-accounts conn) req) => (spec-check ::ok)
               (-> ((controllers/get-available-accounts conn) req) body :accounts) => (just expt)))

       (fact "we should be able to get account"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   req {:route-params {:account-id "2-sec-1"} :headers {"accept" "application/edn"}}]
               ((controllers/get-account conn) req) => (spec-check ::ok)
               (body ((controllers/get-account conn) req)) => {:account {:account-public-key "2-sec-1" :amount 200 :sec-id :sec-1}
                                                               :sec-url "/api/1/ledger/securities/sec-1"}))

       (fact "the interest should be payed when getting the account"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   req {:route-params {:account-id "2-sec-1"} :headers {"accept" "application/edn"}}]
               ((controllers/get-account conn) req) => (spec-check ::ok)
               (body ((controllers/get-account conn) req)) => {:account {:account-public-key "2-sec-1" :amount 200 :sec-id :sec-1}
                                                               :sec-url "/api/1/ledger/securities/sec-1"}))


       (fact "we should not get an account if it does not exists"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   req {:route-params {:account-id "2-sec-10"} :headers {"accept" "application/edn"}}]
               ((controllers/get-account conn) req) => (spec-check ::status-404))))


(facts "about interest computation"
       (fact "about very basic interest rates"
             (let [amount 1000
                   interest-rate (* 10 100)                 ;; daily 10% interest rates
                   negative-interest-rate (- interest-rate)
                   start #inst"2020-09-05T00:00:00.000-00:00"
                   start+1d #inst"2020-09-06T00:00:00.000-00:00"
                   start+2d #inst"2020-09-07T00:00:00.000-00:00"]
               (data-layer/compute-simple-face-value amount interest-rate start start) => (roughly amount)
               (data-layer/compute-simple-face-value amount interest-rate start start+1d) => (roughly 1105.1670808)
               (data-layer/compute-simple-face-value amount interest-rate start start+2d) => (roughly 1221.3942766)
               (data-layer/compute-simple-face-value amount negative-interest-rate start start+2d) => (roughly 818.7250672)
               (data-layer/compute-simple-face-value amount 0 start start+2d) => (roughly amount)))
       (fact "split-interest-rates"
             (let [d-1d #inst"2020-09-04T00:00:00.000-00:00"
                   d-½d #inst"2020-09-04T12:00:00.000-00:00"
                   d #inst"2020-09-05T00:00:00.000-00:00"
                   d+½d #inst"2020-09-05T12:00:00.000-00:00"
                   d+1d #inst"2020-09-06T00:00:00.000-00:00"
                   d+1½d #inst"2020-09-06T12:00:00.000-00:00"
                   d+2d #inst"2020-09-07T00:00:00.000-00:00"
                   d+2½d #inst"2020-09-07T12:00:00.000-00:00"
                   d+3d #inst"2020-09-08T00:00:00.000-00:00"
                   d+3½d #inst"2020-09-08T12:00:00.000-00:00" ;;
                   interest-rates [[10 d] [15 d+1d] [20 d+2d]]]
               (data-layer/split-interest-rates interest-rates d+½d d+3d) => [[10 d+½d d+1d] [15 d+1d d+2d] [20 d+2d d+3d]]
               (data-layer/split-interest-rates interest-rates d+3d d+3½d) => [[20 d+3d d+3½d]]
               (data-layer/split-interest-rates interest-rates d-1d d-½d) => [[0 d-1d d-½d]]
               (data-layer/split-interest-rates interest-rates d d+½d) => [[10 d d+½d]]
               (data-layer/split-interest-rates interest-rates d+2½d d+2½d) => [[20 d+2½d d+2½d]]))
       (fact "compute-face-value"
             (let [d-1d #inst"2020-09-04T00:00:00.000-00:00"
                   d-½d #inst"2020-09-04T12:00:00.000-00:00"
                   d #inst"2020-09-05T00:00:00.000-00:00"
                   d+½d #inst"2020-09-05T12:00:00.000-00:00"
                   d+1d #inst"2020-09-06T00:00:00.000-00:00"
                   d+1½d #inst"2020-09-06T12:00:00.000-00:00"
                   d+2d #inst"2020-09-07T00:00:00.000-00:00"
                   d+2½d #inst"2020-09-07T12:00:00.000-00:00"
                   d+3d #inst"2020-09-08T00:00:00.000-00:00"
                   d+3½d #inst"2020-09-08T12:00:00.000-00:00"
                   amount 10000000
                   interest-rates [[10 d] [15 d+1d] [20 d+2d]]]
               (data-layer/compute-face-value amount interest-rates d+½d d+½d) => amount
               (data-layer/compute-face-value amount interest-rates d-1d d-½d) => amount
               (data-layer/compute-face-value amount interest-rates d-1d d+3d) => 10045101))
       (fact "we had bugs... we need to fix them"
             (let [deposit-date #inst"2020-09-09T18:49:47.205-00:00"
                   value-date #inst"2020-09-09T18:52:56.760-00:00"
                   amount 105402100
                   interest-rates (list [1000 #inst "2020-09-09T18:47:34.152-00:00"])]
               (data-layer/compute-face-value amount interest-rates deposit-date value-date) => 105424060)))


;;    4. transfer sec from one account to the other.

(defn get-account-eids [db & owner-public-keys]
  (let [q (map #(vector :skirmish.ledger/owner-public-key %) owner-public-keys)
        ret (d/pull-many db '[:db/id] q)]
    (map :db/id ret)))

(facts "about transactions"
       (fact "about transferring an amount from one wallet to another"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   transaction {:from-account-id "1-sec-1"
                                :to-account-id   "2-sec-1"
                                :amount          50}
                   db (d/db conn)
                   [eid1 eid2] (get-account-eids db "1-sec-1" "2-sec-1")
                   expt #{[:db/add eid1 :skirmish.ledger/amount 50]
                          [:db/add eid2 :skirmish.ledger/amount 250]}]
               (data-layer/create-transactions db [transaction]) => (just expt)))
       (fact "about transferring an amount from one wallet to another"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   transaction {:from-account-id "1-sec-1"
                                :to-account-id   "1-sec-1"
                                :amount          45}
                   db (d/db conn)
                   [eid1] (get-account-eids db "1-sec-1")
                   expt #{[:db/add eid1 :skirmish.ledger/amount 100]}]
               (data-layer/create-transactions db [transaction]) => (just expt)))
       (fact "about transferring amounts between three wallets"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   transaction-1 {:from-account-id "1-sec-1"
                                  :to-account-id   "2-sec-1"
                                  :amount          50}
                   transaction-2 {:from-account-id "2-sec-1"
                                  :to-account-id   "3-sec-1"
                                  :amount          250}
                   db (d/db conn)
                   [eid1 eid2 eid3] (get-account-eids db "1-sec-1"  "2-sec-1"  "3-sec-1")
                   expt #{[:db/add eid1 :skirmish.ledger/amount 50]
                          [:db/add eid2 :skirmish.ledger/amount 0]
                          [:db/add eid3 :skirmish.ledger/amount 550]}]
               (data-layer/create-transactions db [transaction-1 transaction-2]) => (just expt)))
       (fact "about transaction between different assets types"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   transaction {:from-account-id "1-sec-1"
                                :to-account-id   "1-sec-2"
                                :amount 50}
                   db (d/db conn)]
               (data-layer/create-transactions db [transaction]) => (throws #"different underlying securities")))
       (fact "about transaction with negative results"
             (let [conn (create-some-securities-and-accounts (create-conn) 10)
                   transaction {:from-account-id "1-sec-1"
                                :to-account-id   "2-sec-1"
                                :amount 500}
                   db (d/db conn)]
               (data-layer/create-transactions db [transaction]) => (throws #"negative amount in a wallet"))))

