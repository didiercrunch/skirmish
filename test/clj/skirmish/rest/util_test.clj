(ns skirmish.rest.util-test
  (:require [skirmish.rest.util :refer [build-response]]
            [midje.sweet :refer :all]
            [clojure.test :refer :all]))

(defn expt-response [body content-type status]
  {:body body
   :headers {"Content-Type" content-type}
   :status status})

(facts "the building the response"
       (fact "building the response in edn"
         (let [content-type "application/edn"
               request {:headers {"accept" content-type}}
               response "foo"
               expt (expt-response "foo" "application/edn" 200)]
           (build-response request response) => expt))
       (fact "building the response in edn"
             (let [content-type "application/edn"
                   request {:headers {"accept" content-type}}
                   response {:foo [1 2 3]}
                   expt (expt-response "{:foo [1 2 3]}" "application/edn" 200)]
               (build-response request response) => expt))
       (fact "building the response in edn"
             (let [content-type "application/edn,application/json"
                   request {:headers {"accept" content-type}}
                   response {:foo [1 2 3]}
                   expt (expt-response "{:foo [1 2 3]}" "application/edn" 200)]
               (build-response request response) => expt))
       (fact "building the response in json"
             (let [content-type "text/html,application/xhtml+xml,application/json,application/xml;q=0.9,*/*;q=0.8"
                   request {:headers {"accept" content-type}}
                   response {:foo [1 2 3]}
                   expt (expt-response "{\"foo\":[1,2,3]}" "application/json" 200)]
               (build-response request response) => expt))
       (fact "My browser should receive json"
           (let [content-type "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
                 request {:headers {"accept" content-type}}
                 response {:foo [1 2 3]}
                 expt (expt-response "{\"foo\":[1,2,3]}" "application/json" 200)]
             (build-response request response) => expt)))


