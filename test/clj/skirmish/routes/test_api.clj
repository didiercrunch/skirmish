(ns skirmish.routes.test-api
  (:require [midje.sweet :refer :all]
            [skirmish.routes.api :refer :all]))

(def my-ledger-routes {
                       :name :ledger
                       :path "/api/1/ledger"
                       :routes [{:name :home :path "/"}
                                {:name :accounts :path "/accounts"}
                                {:name :account :path "/accounts/:account-id"}]})
(def wui-routes {
                 :name :wui
                 :domain "http://localhost:1534"
                 :path "/api/1/wui"
                 :routes [{:name :home :path "/home"}]})


(def my-all-routes [my-ledger-routes wui-routes])

(facts "About routes"
       (fact "it should be easy to get routes"
             (get-compojure-path my-all-routes :ledger/accounts) => "/accounts")

       (fact "it should be able to retrieve routes"
             (get-path-for my-all-routes :ledger/accounts) => "/api/1/ledger/accounts"
             (get-path-for my-all-routes :ledger/account {:account-id "one-two"}) => "/api/1/ledger/accounts/one-two"
             (get-path-for my-all-routes :wui/home) => "http://localhost:1534/api/1/wui/home")

       (fact "it should be able to retrieve the contect"
             (get-context my-all-routes :ledger) => "/api/1/ledger"))
