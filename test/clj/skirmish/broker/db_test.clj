(ns skirmish.broker.db-test
  (:require [midje.sweet :refer :all]
            [orchestra.spec.test :as st]
            [datomic.api :as d]
            [skirmish.broker.db :refer [create-connection]]
            [skirmish.broker.broker :refer [create-account]]))

(st/instrument)

(defn create-conn []
  (let [mem-url "datomic:mem://skirmish"]
    (d/delete-database mem-url)
    (create-connection mem-url)))

(facts "about the db functions."
       (fact "should be able to create new users"
             (let [conn (create-conn)
                   email "foo@gmail.com"
                   account-id #uuid"dc0d1123-e659-4e8a-b38b-ece520f4c4d0"
                   account (create-account account-id :dapm)
                   _ @(d/transact conn [[:skirmish.broker.user/create-user email account]])
                   db (d/db conn)
                   user (d/pull db '[* {:skirmish.broker.user/accounts [*]}] [:skirmish.broker.user/email email])]
               (:skirmish.broker.user/email user) => email
               (-> user :skirmish.broker.user/accounts first :skirmish.broker.account/id) => account-id)))