(ns skirmish.broker.broker-test
  (:require [midje.sweet :refer :all]
            [orchestra.spec.test :as st]
            [skirmish.broker.broker :refer [add-account-urls
                                            create-user-seed-from-email
                                            generate-amount-for-new-user]]))

(st/instrument)


(facts "the broker util functions"
       (fact "it should be able to add url to accounts"
             (let [user {:skirmish.broker.user/accounts [{:skirmish.broker.account/id "foo"}]}
                   ret {:skirmish.broker.user/accounts [{:skirmish.broker.account/id "foo"
                                                         :skirmish.broker.account/url "http://localhost:1604/api/1/ledger/accounts/foo"}]}]
               (add-account-urls user) => ret)))

(facts "about the new users"
       (fact "we should add some random amount of damp in their account"
             (let [random-values (map generate-amount-for-new-user (range 10000))
                   sum (reduce + random-values)
                   mean (/ sum (count random-values))
                   min-data (apply min random-values)
                   max-data (apply max random-values)]
               (int mean) => 100027427
               min-data => 78422400
               max-data => 120128000))

       (fact "we should generate the random seed by the email"
             (create-user-seed-from-email "foo@gmail.com") => 399122929
             (create-user-seed-from-email "boo@gmail.com") => 1433675645))


