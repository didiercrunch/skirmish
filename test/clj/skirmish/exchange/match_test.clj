(ns skirmish.exchange.match-test
  (:require [midje.sweet :refer :all]
            [skirmish.exchange.match :refer :all]))


(def PersistentSortedList? (comp #(= % skirmish.PersistentSortedList) type))


(facts "matching buy order in sell book"
  (fact "Simplest case I."
        (let [sell-book (create-sell-book {:type :sell :quantity 10 :price 2 :book.order/id 101})
              order {:type :buy :quantity 10 :price 2 :book.order/id 102}
              [trades new-book] (match-order-in-book sell-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 10 :price 2 :book-id 101 :order-id 102 :type :sell}]))

  (fact "Simplest case II."
        (let [sell-book (create-sell-book {:type :sell :quantity 10 :price 2 :book.order/id 103})
              order {:type :buy :quantity 10 :price 3 :book.order/id 104}
              [trades new-book] (match-order-in-book sell-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 10 :price 2 :book-id 103 :order-id 104 :type :sell}]))
  (fact "Simplest case III."
        (let [sell-book (create-sell-book {:type :sell :quantity 10 :price 2 :book.order/id 105})
              order {:type :buy :quantity 8 :price 2 :book.order/id 106}
              [trades new-book] (match-order-in-book sell-book order)]
          new-book => [{:type :sell :quantity 2 :price 2 :book.order/id 105}]
          trades => [#:book.order{:quantity 8 :price 2 :book-id 105 :order-id 106 :type :sell}]))
  (fact "Simplest case IV."
        (let [sell-book (create-sell-book {:type :sell :quantity 2 :price 12 :book.order/id 107} {:type :sell :quantity 8 :price 13 :book.order/id 108})
              order {:type :buy :quantity 10 :price 13 :book.order/id 109}
              [trades new-book] (match-order-in-book sell-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 2 :price 12 :book-id 107 :order-id 109 :type :sell}
                     #:book.order{:quantity 8 :price 13 :book-id 108 :order-id 109 :type :sell}]))
  (fact "Simplest case V."
        (let [sell-book (create-sell-book {:type :sell :quantity 2 :price 12 :book.order/id 113} {:type :sell :quantity 8 :price 13 :book.order/id 114})
              order {:type :buy :quantity 9 :price 13 :book.order/id 115}
              [trades new-book] (match-order-in-book sell-book order)]
          new-book => (create-sell-book {:type :sell :quantity 1 :price 13 :book.order/id 114})
          trades => [#:book.order{:quantity 2 :price 12 :book-id 113 :order-id 115 :type :sell}
                     #:book.order{:quantity 7 :price 13 :book-id 114 :order-id 115 :type :sell}]))
  (fact "Simplest case VI."
        (let [sell-book (create-sell-book {:type :sell :quantity 2 :price 12 :book.order/id 116} {:type :sell :quantity 8 :price 13 :book.order/id 117})
              order {:type :buy :quantity 21 :price 13 :book.order/id 118}
              [trades new-book leftover-order] (match-order-in-book sell-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 2 :price 12 :book-id 116 :order-id 118 :type :sell}
                     #:book.order{:quantity 8 :price 13 :book-id 117 :order-id 118 :type :sell}]
          leftover-order => {:type :buy :quantity 11 :price 13 :book.order/id 118}))
  (fact "Simplest case VII."
        (let [sell-book (create-sell-book {:type :sell :quantity 2 :price 12 :book.order/id 116} {:type :sell :quantity 8 :price 13 :book.order/id 117})
              order {:type :buy :quantity 21 :price 10 :book.order/id 118}
              [trades new-book leftover-order] (match-order-in-book sell-book order)]
          new-book => sell-book
          trades => empty?
          leftover-order => order)))


(facts "matching sell order in buy book"
  (fact "Simplest case I."
        (let [buy-book (create-buy-book {:type :buy :quantity 10 :price 2 :book.order/id 101})
              order {:type :sell :quantity 10 :price 2 :book.order/id 102}
              [trades new-book] (match-order-in-book buy-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 10 :price 2 :book-id 101 :order-id 102 :type :buy}]))

  (fact "Simplest case II."
        (let [buy-book (create-buy-book {:type :buy :quantity 10 :price 4 :book.order/id 103})
              order {:type :sell :quantity 10 :price 3 :book.order/id 104}
              [trades new-book] (match-order-in-book buy-book order)]
          new-book => []
          trades => [#:book.order{:quantity 10 :price 4 :book-id 103 :order-id 104 :type :buy}]))

  (fact "Simplest case III."
        (let [buy-book (create-buy-book {:type :buy :quantity 10 :price 2 :book.order/id 105})
              order {:type :sell :quantity 8 :price 2 :book.order/id 106}
              [trades new-book] (match-order-in-book buy-book order)]
          new-book => [{:type :buy :quantity 2 :price 2 :book.order/id 105}]
          trades => [#:book.order{:quantity 8 :price 2 :book-id 105 :order-id 106 :type :buy}]))

  (fact "Simplest case IV."
        (let [buy-book (create-buy-book {:type :buy :quantity 2 :price 14 :book.order/id 107}
                                        {:type :buy :quantity 8 :price 13 :book.order/id 108})
              order {:type :sell :quantity 10 :price 13 :book.order/id 109}
              [trades new-book] (match-order-in-book buy-book order)]
          new-book => []
          trades => [#:book.order{:quantity 2 :price 14 :book-id 107 :order-id 109 :type :buy}
                     #:book.order{:quantity 8 :price 13 :book-id 108 :order-id 109 :type :buy}]))

  (fact "Simplest case V."
        (let [buy-book (create-buy-book {:type :buy :quantity 2 :price 14 :book.order/id 113}
                                        {:type :buy :quantity 8 :price 13 :book.order/id 114})
              order {:type :sell :quantity 9 :price 13 :book.order/id 115}
              [trades new-book] (match-order-in-book buy-book order)]
          new-book => (create-buy-book {:type :buy :quantity 1 :price 13 :book.order/id 114})
          trades => [#:book.order{:quantity 2 :price 14 :book-id 113 :order-id 115 :type :buy}
                     #:book.order{:quantity 7 :price 13 :book-id 114 :order-id 115 :type :buy}]))

  (fact "Simplest case VI."
        (let [sell-book (create-buy-book {:type :buy :quantity 2 :price 14 :book.order/id 116}
                                         {:type :buy :quantity 8 :price 13 :book.order/id 117})
              order {:type :sell :quantity 21 :price 13 :book.order/id 118}
              [trades new-book leftover-order] (match-order-in-book sell-book order)]
          new-book => empty?
          trades => [#:book.order{:quantity 2 :price 14 :book-id 116 :order-id 118 :type :buy}
                     #:book.order{:quantity 8 :price 13 :book-id 117 :order-id 118 :type :buy}]
          leftover-order => {:type :sell :quantity 11 :price 13 :book.order/id 118})))

(facts "We should write tests for each bug found"
  (fact "book type should always be book.PersistentSortedList"
    (let [book (create-buy-book)
          order {:type :sell :quantity 21 :price 13 :book.order/id 118}
          [_ new-book _] (match-order-in-book book order)]
      (count new-book) => 0
      (type new-book) => skirmish.PersistentSortedList)))